# argus.py to compare ICAL and IRR files

# get collections
from collections import Counter
import numpy

# import hydras
from hydras import Hydra

# import matplotlib
from matplotlib import pyplot


# define matching function
def spy():
    """Compare the parameters in an IRR file and Cal file.

    Arguments:
        None

    Returns:
        None
    """

    # generate hydra instances
    hydro = Hydra('studies/irradiances', 'studies/irradiances/data')
    hydri = Hydra('studies/irradiances', 'studies/irradiances/data')

    # ingest paths
    hydro.ingest(hydro.paths[0])
    hydri.ingest(hydri.paths[5])

    # for each irradiance data set
    pairs = []
    for feature in hydri:

        # get first and last name members
        search = '/'.join([feature.route[0], feature.name])

        # find in cal file
        matches = hydro.dig(search) + hydro.dig(search.replace('irradiance', 'irradiance_avg'))
        matches.sort(key=lambda match: feature.name.replace('irradiance', 'irradiance_avg') == match.name, reverse=False)
        matches.sort(key=lambda match: feature.name == match.name, reverse=True)

        # append pair
        pairs.append((feature, matches, len(matches)))

    # sort pairs
    pairs.sort(key=lambda pair: pair[2], reverse=True)

    # check for equality
    passes = [pair for pair in pairs if numpy.array(pair[0].distil() == pair[1][0].distil()).all()]

    # check for equality
    fails = [pair for pair in pairs if not numpy.array(pair[0].distil() == pair[1][0].distil()).all()]
    fails.sort(key=lambda triplet: triplet[2])

    # check for singlets
    singlets = [fail for fail in fails if len(fail[0].shape) < 3]
    multiplets = [fail for fail in fails if len(fail[0].shape) > 2]


    print('passes: {}'.format(len(passes)))
    print('singlets: {}'.format(len(singlets)))
    print('multiplets: {}'.format(len(multiplets)))

    # go through singlets
    print('\nsinglets:')

    # sort singlets by matches or mismatces
    accepts = []
    rejects = []
    errors = []
    for single in singlets:

        # print the shapes
        #print(single[0].name, single[1][0].name, single[0].shape, single[1][0].shape)

        # try to
        try:

            # match based on index 16
            if single[0].distil()[0, 0] == single[1][0].distil()[0, 16]:

                # put in accepts
                accepts.append(single)

            # otherwise
            else:

                # put in rejects
                rejects.append(single)

        # unless an index error
        except IndexError:

            # in which case, add to errors
            errors.append(single)

    # print lengths
    print('accepts: {}'.format(len(accepts)))
    print('rejects: {}'.format(len(rejects)))
    print('errors: {}'.format(len(errors)))

    # go through multiplets
    print('\nmultiplets:')

    # sort multiplets by matches or mismatces
    acceptos = []
    rejectos = []
    erroros = []
    for multiple in multiplets:

        # print the shapes
        #print(multiple[0].name, multiple[1][-1].name, multiple[0].shape, multiple[1][-1].shape)

        # try to
        try:

            # match based on index 16
            if numpy.array(multiple[0].distil().squeeze() == multiple[1][-1].distil().squeeze()).all():

                # put in accepts
                acceptos.append(multiple)

            # otherwise, try flipping band1
            elif numpy.array(numpy.flip(multiple[0].distil().squeeze(), -1) == multiple[1][-1].distil().squeeze()).all():

                # put in accepts
                acceptos.append(multiple)

            # otherwise
            else:

                # put in rejects
                rejectos.append(multiple)

        # unless an index error
        except IndexError:

            # in which case, add to errors
            erroros.append(multiple)

    # print lengths
    print('accepts: {}'.format(len(acceptos)))
    print('rejects: {}'.format(len(rejectos)))
    print('errors: {}'.format(len(erroros)))

    for reject in rejectos + errors:

        # print the shapes
        print(reject[0].name)


def waver():
    """Compare wavelength shifts with temperature.

    Arguments:
        None

    Returns:
        None
    """

    # make instance
    hydri = Hydra('', 'studies/irradiances/data')

    # collect data
    temperatures = []
    wavelengths = []

    # get coeffs
    coefficients = []

    # go through IRR files
    for path in hydri.paths[5:]:

        # ingest
        hydri.ingest(path)

        # get opb temp and wavelength
        temperature = float(hydri.dig('temp_opb')[2].distil().squeeze())
        temperatures.append(temperature)

        # get wavelength
        wavelength = hydri.dig('wavelength')[2].distil().squeeze()[0, 0]
        wavelengths.append(wavelength)

        # get coefficient
        coefficient = hydri.dig('wavelength_coefficient')[2].distil().squeeze()[0, 0]
        coefficients.append(coefficient)

    # print
    print('temperatures: {}'.format(temperatures))
    print('wavelengths: {}'.format(wavelengths))
    print('coefficients: {}'.format(coefficients))

    # plot
    pyplot.clf()
    #pyplot.plot(temperatures, wavelengths, 'ro-')
    pyplot.plot(coefficients, temperatures, 'go-')
    pyplot.savefig('tempo.png')
    pyplot.clf()

    return None


def calculate(coefficients, references, pixels):
    """Calculate wavelengths from polynomial coefficients.

    Arguments:
        coefficients: numpy array, polynomial coefficients
        references: numpy array, reference columns
        pixels: int, number of spectral pixels

    Returns:
        numpy array, calculated wavelengths
    """

    # get relevant dimensions
    orbits, rows, degree = coefficients.shape
    print('orbits: {}, rows: {}, pixels: {}'.format(orbits, rows, pixels))

    # create tensor of spectral pixel indices
    columns = numpy.linspace(0, pixels - 1, num=pixels)
    image = numpy.array([columns] * rows)
    indices = numpy.array([image] * orbits)

    # expand tensor of column references
    references = numpy.array([references] * rows)
    references = numpy.array([references] * pixels)
    references = references.transpose(2, 1, 0)

    # subtract for column deltas
    deltas = indices - references

    # expand deltas along coefficient powers
    powers = numpy.array([deltas ** power for power in range(degree)])
    powers = powers.transpose(1, 2, 3, 0)

    # expand coefficients along pixels
    coefficients = numpy.array([coefficients] * pixels)
    coefficients = coefficients.transpose(1, 2, 0, 3)

    # multiple and sum to get wavelengths
    wavelengths = powers * coefficients
    wavelengths = wavelengths.sum(axis=3)

    return wavelengths



