# caladriuss.py for the Caladrius class to make OMI Rawflux hdf5 files

# import general tools
import sys
import re
import ast

# import yaml for reading configuration file
import yaml

# import time and datetime
import datetime
import calendar

# import math functions
import math
import numpy

# import local classes
from features import Feature
from formulas import Formula
from hydras import Hydra


# try to
try:

    # import sci-kit learn for regressions
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures

# unless it is not installed
except ImportError:

    # in which case, nevermind
    print('\n** sci-kit learn not installed, regressions not possible **')

# try to
try:

    # import matplotlib for plots
    from matplotlib import pyplot

# unless it is not installed
except ImportError:

    # in which case, nevermind
    print('\n** matplotlib not installed, plotting not possible **')


# class Caladrius to do OMI data reduction
class Caladrius(Hydra):
    """Caladrius class to generate Calibration records.

    Inherits from:
        list
    """

    def __init__(self, sink='', source='', start='', finish='', **options):
        """Initialize a Caladrius instance.

        Arguments:
            sink: str, filepath for data dump
            source: str, filepath of source files
            start: str, date-based subdirectory
            finish: str, date-based subdirectory
            **options: kwargs for other options

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self, source, start, finish)
        self.sink = sink

        # configure specifics
        self.mode = ''
        self.track = 0
        self.diffusers = {}
        self.rows = []
        self.parameters = []

        # set other boolean options
        self.postpone = options.setdefault('postpone', False)
        self.force = options.setdefault('force', False)
        self.extend = options.setdefault('extend', False)
        self.cube = options.setdefault('cube', False)
        self.analyze = options.setdefault('analyze', False)

        # set fusion frequency
        self.throttle = 1

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # display contents
        self._tell(self.paths)

        # create representation
        representation = ' < Caladrius instance at: {} -> {} >'.format(self.source, self.sink)

        return representation

    def _adjust(self, irradiances, distances):
        """Adjust irradiance measurements for earth sun distance.

        Arguments:
            irradiances: numpy array
            distances: numpy array

        Returns:
            numpy array
        """

        # expand distances
        expansion = numpy.ones(irradiances.shape)
        expansion[:] = distances ** 2

        # create adjustments
        adjustments = irradiances * expansion

        return adjustments

    def _bin(self, tensor, size, axis=0):
        """Perform binning of a tensor by averaging over some axis.

        Arguments:
            tensor: numpy array
            size: int, size of bins
            axis: axis over which to bin

        Returns:
            numpy array
        """

        # swap axes to put main axis first
        tensor = tensor.swapaxes(0, axis)

        # get number of bins
        shape = list(tensor.shape)
        bins = math.floor(shape[0] / size)
        shape[0] = bins

        # create new emtpy array
        array = numpy.zeros(tuple(shape))

        # populate array with averages, skippping last bin
        for index in range(bins):

            # create average of slice
            slice = numpy.mean(tensor[index * size: size + index * size], axis=0)

            # add to array
            array[index] = numpy.array([slice])

        # reswap axes
        array = array.swapaxes(0, axis)

        return array

    def _blur(self):
        """Perform wavelength binning on irradiance ratios.

        Arguments:
            None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/compressions'.format(self.sink))

        # get all aggregate files
        directory = '{}/interpolations'.format(self.sink)
        paths = self._see(directory)

        # go through each path
        for path in paths:

            # ingest the path
            self.ingest(path)

            # extract band and diffuser names
            band = re.search('BAND[1-3]', path).group()
            diffuser = re.search('|'.join(self.diffusers.values()), path).group()

            # timestamp
            self._stamp('\nperforming compressions for {}, {}...'.format(diffuser, band), initial=True)

            # begin formula
            formula = Formula()

            # add independent variables
            independents = self.apply(lambda feature: 'IndependentVariables' in feature.slash)
            [formula.formulate(feature.name) for feature in independents]

            # bin initial ratios spectrally
            def binning(tensor): return self._bin(tensor, 5, 2)
            formula.formulate('initial_irradiance_ratios', binning, 'binned_initial_ratios')

            # bin pixel grid as well
            def binning(tensor): return self._bin(tensor, 5, 0)
            formula.formulate('PixelGrid', binning, 'BinnedPixelGrid')

            # perform cascade to calculate all new features
            features = self.cascade(formula)

            # update route with Categories designation and link designation
            categories = self.apply(lambda feature: 'Categories' in feature.slash, features=features)
            [feature.update({'link': True}) for feature in categories]

            # create destination name and stash featues
            now = self._note()
            destination = '{}/compressions/OMI_Compressions_{}_{}_{}.h5'.format(self.sink, band, diffuser, now)
            self._stash(features, destination, 'Data')

            # timestamp
            self._stamp('compressed.')

        return None

    def _calculate(self, coefficients, references, pixels):
        """Calculate wavelengths from polynomial coefficients.

        Arguments:
            coefficients: numpy array, polynomial coefficients
            references: numpy array, reference columns
            pixels: int, number of spectral pixels

        Returns:
            numpy array, calculated wavelengths
        """

        # get relevant dimensions
        orbits, rows, degree = coefficients.shape
        print('orbits: {}, rows: {}, pixels: {}'.format(orbits, rows, pixels))
        self._stamp('calculating wavelengths...', initial=True)

        # create tensor of spectral pixel indices
        columns = numpy.linspace(0, pixels - 1, num=pixels)
        image = numpy.array([columns] * rows)
        indices = numpy.array([image] * orbits)

        # expand tensor of column references
        references = numpy.array([references] * rows)
        references = numpy.array([references] * pixels)
        references = references.transpose(2, 1, 0)

        # subtract for column deltas
        deltas = indices - references

        # expand deltas along coefficient powers
        powers = numpy.array([deltas ** power for power in range(degree)])
        powers = powers.transpose(1, 2, 3, 0)

        # expand coefficients along pixels
        coefficients = numpy.array([coefficients] * pixels)
        coefficients = coefficients.transpose(1, 2, 0, 3)

        # multiple and sum to get wavelengths
        wavelengths = powers * coefficients
        wavelengths = wavelengths.sum(axis=3)

        # timestamp
        self._stamp('calculated.')

        return wavelengths

    def _calendarize(self, irradiances, years, months):
        """Bin irradiance measurements by month.

        Arguments:
            irradiances: numpy array
            years: numpy array
            months: numpy array

        Returns:
            tuple of numpy arrays
        """

        # get unique years and months
        uniques = numpy.unique(years).tolist()
        uniquesii = numpy.unique(months).tolist()
        uniques.sort()
        uniquesii.sort()

        # for each year
        monthlies = []
        times = []
        for year in uniques:

            # and each month
            for month in uniquesii:

                # create masks
                mask = years == year
                maskii = months == month
                composite = mask * maskii

                # if non zero
                if composite.sum() > 0:

                    # subset irradiances
                    subset = irradiances[composite].mean(axis=0)
                    monthlies.append(subset)

                    # create timestamp from year and month
                    clock = datetime.datetime(int(year), int(month), 1, 0, 0)
                    stamp = float(calendar.timegm(clock.utctimetuple()))
                    milliseconds = stamp * 1000
                    times.append(milliseconds)

        # create arrays
        monthlies = numpy.array(monthlies)
        times = numpy.array(times)

        return monthlies, times

    def _compose(self, mantissas, exponents):
        """Compose irradiances from mantissas and exponenets.

        Arguments:
            mantissas: numpy array
            exponents: numpy array

        Returns:
            numpy array
        """

        # squeeze matrices
        mantissas = mantissas.squeeze()
        exponents = exponents.squeeze()

        # get conversion factor
        factor = 10000 / 6.02214076e23

        # raise exponents
        tens = numpy.ones(exponents.shape) * 10
        powers = numpy.power(tens, exponents)

        # construct irradiances
        irradiances = factor * mantissas * powers
        irradiances = irradiances.squeeze()

        return irradiances

    def _configure(self):
        """Configure specific OMI parameter attributes for extraction.

        Arguments:
            None

        Returns:
            None
        """

        # open yaml file
        with open('configuration.yaml', 'r') as pointer:

            # and read contents
            configuration = yaml.safe_load(pointer)

        # set attributes
        self.mode = configuration['mode']
        self.diffusers = configuration['diffusers']

        # subset the parameters marked by X
        parameters = configuration['parameters']
        parameters = [parameter.split()[0] for parameter in parameters if 'X' in parameter]
        self.parameters = parameters

        return None

    def _consolidate(self, averages):
        """Make plots with all bands.

        Arguments:
            averages: dict

        Returns:
            None
        """

        # make the directory
        self._make('{}/averages'.format(self.sink))

        # set up band colors
        colors = {'BAND1': 'k', 'BAND2': 'm', 'BAND3': 'b'}
        styles = {'volume': '-', 'regular': '--', 'backup': '-.', 'components': ':'}

        # make row averages plot
        lines = []
        for band, diffuser in averages.items():

            # go through each diffuser
            for description, data in diffuser.items():

                # create line
                ordinate = averages[band][description]['rows']
                abscissa = averages[band][description]['grid']
                color = colors[band]
                style = [ink for tag, ink in styles.items() if tag in description][0]
                label = ['{}_{}'.format(band, description)]
                label = None
                line = [abscissa, ordinate, color + style, label]
                lines.append(line)

        # make texts
        title = 'row average degradation rates'
        dependent = 'wavelength'
        independent = '% degradation / year'
        texts = [title, dependent, independent]

        # get destination
        destination = '{}/averages/row_averaged_degradation_rates.png'.format(self.sink)

        # draw
        self._draw(lines, texts, destination)

        # make row averages plot
        lines = []
        for band, diffuser in averages.items():

            # go through each diffuser
            for description, data in diffuser.items():

                # create line
                ordinate = averages[band][description]['spectrum']
                abscissa = [row for row in range(len(ordinate))]
                color = colors[band]
                style = [ink for tag, ink in styles.items() if tag in description][0]
                label = ['{}_{}'.format(band, description)]
                label = None
                line = [abscissa, ordinate, color + style, label]
                lines.append(line)

        # make texts
        title = 'spectral average degradation rates'
        dependent = 'row'
        independent = '% degradation / year'
        texts = [title, dependent, independent]

        # get destination
        destination = '{}/averages/spectral_averaged_degradation_rates.png'.format(self.sink)

        # draw
        self._draw(lines, texts, destination)

        return None

    def _draw(self, lines, texts, destination):
        """Draw a group of lines.

        Arguments:
            lines: list of tuples
            texts: list of str
            destination: str, filepath

        Returns:
            None
        """

        # begin plot
        pyplot.clf()
        figure = pyplot.figure()

        # create a grid for combining subplots
        grid = figure.add_gridspec(ncols=1, nrows=3, width_ratios=[1], height_ratios=[2, 10, 2])

        # defie the axes
        axes = [figure.add_subplot(grid[0, :])]
        axes.append(figure.add_subplot(grid[1, :]))
        axes.append(figure.add_subplot(grid[2, :]))

        # adjust margins
        figure.subplots_adjust(hspace=0.0, wspace=0.5)

        # determine all entries
        data = [float(datum) for line in lines for datum in line[1]]

        # define quantile cutoffs for head and tail
        quantile = 2
        maximum = max(data)
        upper = numpy.percentile(data, 100 - quantile)
        lower = numpy.percentile(data, quantile)
        minimum = min(data)

        # define y range
        breadth = upper - lower
        head = upper + (breadth / 5)
        tail = lower - (breadth / 5)
        top = max([maximum, head + (breadth / 20)])
        bottom = min([minimum, tail - (breadth / 20)])

        # plot all lines
        for abscissa, ordinate, style, label in lines:

            # if there is a label
            if label:

                # plot with a label
                axes[0].plot(abscissa, ordinate, style)
                axes[1].plot(abscissa, ordinate, style, label=label)
                axes[2].plot(abscissa, ordinate, style)

            # otherwise
            else:

                # plot without a label
                axes[0].plot(abscissa, ordinate, style)
                axes[1].plot(abscissa, ordinate, style)
                axes[2].plot(abscissa, ordinate, style)

        # set axis limits
        axes[0].set_ylim(head, top)
        axes[1].set_ylim(tail, head)
        axes[2].set_ylim(bottom, tail)

        # parse text, padding with blanks
        texts = texts + ['', '', '']
        title, independent, dependent = texts[:3]

        # add labels
        axes[0].set_title(title)
        axes[1].set_ylabel(dependent)
        axes[2].set_xlabel(independent)

        # remove xtick labels in head and main
        axes[0].set_xticklabels([" "] * len(axes[1].get_xticks()))
        axes[1].set_xticklabels([" "] * len(axes[1].get_xticks()))

        # add legend
        figure.legend(loc='upper right')

        # save the figure
        pyplot.savefig(destination)
        pyplot.close()

        return None

    def _interpolate(self, irradiances, wavelengths, band):
        """Interpolate irradiances across a wavelength grid.

        Arguments:
            irradiances: numpy array of irradiances
            wavelengths: numpy array of floats, calculated wavelengths
            band: str, name of band

        Returns:
            None
        """

        # if for band 1
        if 'BAND1' in band:

            # flip wavelengths and irradiances because they are backwards
            wavelengths = numpy.flip(wavelengths, axis=2)
            irradiances = numpy.flip(irradiances, axis=2)

        # clip 20 spectral pixels from each end
        wavelengths = wavelengths[:, :, 20:-20]
        irradiances = irradiances[:, :, 20:-20]

        # for each image
        for index, matrix in enumerate(irradiances):

            # transpose to get columns
            columns = matrix.transpose(1, 0)
            for indexii, column in enumerate(columns):

                # create masks for less than zero
                mask = column <= 0
                maskii = column > 0
                average = column[maskii].mean()
                column[mask] = average

                # replace column in matrix
                columns[indexii] = column

            # re transpose matrix
            matrix = columns.transpose(1, 0)
            irradiances[index] = matrix

        # get the start and finish wavelengths based on the shortest and longest wavelengths amongst all rows
        start = wavelengths.min(axis=2).max()
        finish = wavelengths.max(axis=2).min()

        # convert nanometers to angstroms for a grid based on tenths of nanometers
        start = math.ceil(10 * start)
        finish = math.floor(10 * finish)

        # create wavelength grid for entire range at a resolution of tenth of wavelength
        grid = [number / 10 for number in range(start, finish + 1)]
        grid = numpy.array(grid)

        # for each wavelength assignment in the grid
        self._stamp('interpolating irradiances...', initial=True)
        interpolations = []
        for assignment in grid:

            # for particular wavelengths
            if assignment == int(assignment) and int(assignment) % 10 == 0:

                # print status
                print('{} nm...'.format(assignment))

            print('making masks...')

            # make lesser and greater masks
            lesser = wavelengths <= assignment
            greater = wavelengths > assignment

            print('making rolls...')

            # roll the greater mask towards the lesser so trues overlap only left of the assignment
            left = lesser * numpy.roll(greater, -1, axis=2)

            # roll the lesser towards the greater so trues overlap only right of the assignment
            right = numpy.roll(lesser, 1, axis=2) * greater

            # extract initial and final irradiances by multiplyiing with masks
            initial = (irradiances * left)
            final = (irradiances * right)

            print('summing...')

            # collapse dimension by summing (just one entry should be nonzero per row)
            initial = initial.sum(axis=2)
            final = final.sum(axis=2)

            # extract short and long wavelengths
            short = (wavelengths * left).sum(axis=2)
            long = (wavelengths * right).sum(axis=2)

            # calculate rise and run
            rise = final - initial
            run = long - short

            print('interpolating...')

            # apply linear interpolation formula: y = y0 + (x - x0) * (y1 - y0) / (x1 - x0)
            interpolation = initial + (assignment - short) * rise / run
            interpolations.append([interpolation])

        # concatenate and transpose
        self._stamp('concatenating...')
        interpolations = numpy.concatenate(interpolations, axis=0)
        interpolations = interpolations.transpose(1, 2, 0)
        self._stamp('concatenated.')

        return interpolations, grid

    def _polymerize(self, point, coefficients):
        """Calculate a point in a polynomial from the abscissa and coefficients.

        Arguments:
            point: float
            coefficients: list of float

        Returns:
            float
        """

        # expand the point with its coefficients
        expansion = [coefficient * point ** degree for degree, coefficient in enumerate(coefficients)]

        # sum the terms
        polynomial = sum(expansion)

        return polynomial

    def _process(self, job):
        """Extract the processing version information from a job description string.

        Arguments:
            job: str, job description

        Returns:
            dict of strings
        """

        # begin processor
        versions = {'processor': '', 'core': ''}

        # search for processor and core
        processor = re.search('OML1BPDS-[0-9].[0-9].[0-9].[0-9]', job.decode('utf8'))
        core = re.search('core_processor_version = [0-9].[0-9].[0-9].[0-9]{5}', job.decode('utf8'))

        # if processor was found
        if processor:

            # add to versions
            versions['processor'] = processor.group()

        # if core was found
        if core:

            # add to versions
            versions['core'] = core.group()

        return versions

    def _rain(self, percents, times):
        """Perform linear regressions at all wavelengths.

        Arguments:
            percents: numpy array
            times: numpy array

        Returns:
            tuple of numpy arrays
        """

        # perform linear regressions at each gridpoint
        self._stamp('performing linear regressions...')

        # transpose subset to get a stack by wavelength, and go through each
        percents = percents.transpose(1, 2, 0)

        # go through each row
        phalanx = []
        for row in percents:

            # go through each row, which is a time sequence of irradiances
            regressions = []
            for wave in row:

                # construct weights to avoid fill value pixels
                saturation = 10000
                weights = [int(abs(member) < saturation) for member in wave]

                # perform linear regression against the times
                machine, score = self._regress(times, wave, weights)
                intercept, slope = machine.coef_

                # gather up slope and intercept
                regression = {'slopes': slope, 'intercepts': intercept}
                regression.update({'r2_scores': score})

                # add to regressions
                regressions.append(regression)

            # append to phalanx
            phalanx.append(regressions)

        # create numpy arrays
        slopes = numpy.array([[entry['slopes'] for entry in row] for row in phalanx])
        intercepts = numpy.array([[entry['intercepts'] for entry in row] for row in phalanx])
        scores = numpy.array([[entry['r2_scores'] for entry in row] for row in phalanx])

        return slopes, intercepts, scores

    def _rationalize(self, one, two, times, timesii, common):
        """Take the ratio between two sets of irradiance measurements.

        Arguments:
            one: numpy array, numerator irradiances
            two: numpy array, denominator irradiances
            times: times of first array
            timesii: times of second array
            common: common time out of all three diffusers

        Returns:
            numpy array
        """

        # get the first subset
        mask = numpy.isin(times, common)
        subset = one[mask]

        # get the second subset
        mask = numpy.isin(timesii, common)
        subsetii = two[mask]

        # create ratio
        ratio = subset / subsetii

        return ratio

    def _regress(self, abscissa, targets, weights=None, degree=1):
        """Perform polynomial regression for a set of data.

        Arguments:
            abscissa: list of floats, x-axis coordinates
            targets: list of floats, y-axis coordinates
            weights: list of floats, weights for weighted regression
            degree=1: degree of polynomial

        Returns:
            regression object
        """

        # construct weights
        weights = weights or [1.0 for _ in targets]

        # set up polynomial matrix
        polynomial = PolynomialFeatures(degree)
        abscissa = numpy.array(abscissa).reshape(-1, 1)
        matrix = polynomial.fit_transform(abscissa)

        # perform regression
        machine = LinearRegression(fit_intercept=False)
        machine.fit(matrix, targets, sample_weight=weights)

        # get coefficient of deterrmination
        score = machine.score(matrix, targets, sample_weight=weights)

        return machine, score

    def _synchronize(self, one, two, three):
        """Create a common set of times amongst three monthly time bins.

        Arguments:
            one: numpy array
            two: numpy array
            three: numpy array

        Returns:
            numpy array
        """

        # get the intersection of all three times
        intersection = set(one) & set(two) & set(three)

        # sort and create array
        times = list(intersection)
        times.sort()
        times = numpy.array(times)

        return times

    def _wind(self, slopes, grid):
        """Create spectral polynomials based on regression slopes.

        Arguments:
            slopes: numpy array
            grid: numpy array

        Returns:
            tuple of numpy arays
        """

        # perform linear regressions at each gridpoint
        self._stamp('performing polynomial regressions...')

        # go every wavelength
        coefficients = []
        polynomials = []
        for spectrum in slopes:

            # perform regression with degree 3
            machine, score = self._regress(grid, spectrum, degree=3)
            coefficient = list(machine.coef_)
            coefficients.append(coefficient)

            # calculate the polynomial
            polynomial = [self._polymerize(wavelength, coefficient) for wavelength in grid]
            polynomials.append(polynomial)

        # create arrays
        coefficients = numpy.array(coefficients)
        polynomials = numpy.array(polynomials)

        return polynomials, coefficients

    def annihilate(self):
        """Destroy all derived data folders.

        Arguments:
            None

        Returns:
            None
        """

        # destroy folders
        self._clean('{}/gazes'.format(self.sink))
        self._clean('{}/fusions'.format(self.sink))
        self._clean('{}/interpolations'.format(self.sink))
        self._clean('{}/regressions'.format(self.sink))
        self._clean('{}/degradations'.format(self.sink))

        return None

    def bridge(self):
        """Convert pregazes into gazes.

        Arguments:
            None

        Returns:
            None
        """

        # make gazes directory
        self._make('{}/gazes'.format(self.sink))

        # get paths
        paths = self._see('{}/pregazes'.format(self.sink))

        # go through each path
        for path in paths:

            # ingest
            self.ingest(path)

            # get variables and categories
            variables = self.dig('IndependentVariables')
            categories = self.dig('Categories')

            # fill all features
            [feature.fill() for feature in categories + variables]

            # find ic_ic
            identities = self.dig('ic_id', categories)[0]
            code = int(identities.data[0])
            diffuser = self.diffusers[code]

            # convert time from year
            variable = self.dig('OrbitStartTimeFrYr', variables)[0]

            # but first replicate and change name
            replica = variable.copy()
            replica.dub('OrbitStartYearFraction')
            variables.append(replica)

            # transfer to timestamps in milliseconds
            times = variable.data
            fractions = []
            for time in times:

                # get first and last year boundaries
                first = datetime.datetime(math.floor(time), 1, 1).timestamp()
                last = datetime.datetime(math.ceil(time), 1, 1).timestamp()
                decimal = time - math.floor(time)

                # construct fraction, and multiply for milleseconds
                fraction = (first + decimal * (last - first)) * 1000
                fractions.append(fraction)

            # instil the data
            variable.instil(numpy.array(fractions))

            # change all variables to snakecase
            [variable.dub(self._serpentize(variable.name)) for variable in variables]
            [category.update({'link': True}) for category in categories]

            # stash
            destination = path.replace('/pregazes/', '/gazes/').replace('.h5', '_{}.h5'.format(diffuser))
            self._stash(variables + categories, destination, 'Data')

        return None

    def calibrate(self):
        """Calculate the interpolated wavelength grid.

        Arguments:
            None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/interpolations'.format(self.sink))

        # get all aggregate files
        directory = '{}/fusions'.format(self.sink)
        paths = self._see(directory)

        # go through each path
        for path in paths:

            # extract band and diffuser names
            band = re.search('BAND[1-3]', path).group()
            diffuser = re.search('|'.join(self.diffusers.values()), path).group()

            # timestamp
            self._stamp('\nperforming interpolations for {}, {}...'.format(diffuser, band), initial=True)

            # ingest the path
            self.ingest(path)

            # begin formulas
            formula = Formula()

            # add independent variables
            independents = self.apply(lambda feature: 'IndependentVariables' in feature.slash)
            [formula.formulate(feature.name) for feature in independents]

            # calculate total exposure times
            def exposing(exposures, times): return exposures * times
            formula.formulate(['exposure_time_index_zero', 'OrbitTrackLength'], exposing, 'total_exposure')

            # determine number of spectral pixels and calculate the wavelengths
            def calculating(coefficient, column, pixels): return self._calculate(coefficient, column, pixels.shape[-1])
            parameters = ['wavelength_coefficient_elevation_zero', 'wavelength_reference_column']
            parameters += ['irradiance_after_relirr_avg']
            name = 'calculated_wavelengths'
            units = {'units': 'nm', 'description': 'calculated wavelengths'}
            formula.formulate(parameters, calculating, name, None, units)

            # add pixel grid
            def pixelating(wavelengths): return numpy.round(wavelengths.mean(axis=(0, 1)), 1)
            units = {'units': 'nm', 'description': 'column averaged calculated wavelengths'}
            formula.formulate('calculated_wavelengths', pixelating, 'pixel_grid', 'IndependentVariables', units)

            # construct the wavelength grid and interpolate the solar irradiances
            def interpolating(irradiances, wavelengths): return self._interpolate(irradiances, wavelengths, band)
            parameters = ['irradiance_after_relirr_avg', 'calculated_wavelengths']
            names = ['interpolated_irradiances', 'wavelength_grid']
            addresses = [None, 'IndependentVariables']
            attributes = [{'units': 'irradiance', 'description': 'linearly interpolated irradiances'}, {'units': 'nm'}]
            formula.formulate(parameters, interpolating, names, addresses, attributes)

            # add formula for irradiance ratio
            def dividing(tensor): return tensor / tensor[0]
            units = {'units': 'irradiance ratio', 'description': 'interpolated irradiances compared to initial irradiance'}
            formula.formulate('interpolated_irradiances', dividing, 'initial_irradiance_ratios', None, units)

            # add formula for irradiance ratio
            def dividing(tensor): return tensor / tensor[0]
            units = {'units': 'irradiance ratio', 'description': 'irradiances compared to initial irradiance'}
            formula.formulate('irradiance_after_relirr_avg', dividing, 'initial_uninterpolated_ratios', None, units)

            # scale interpolations to initial measurement for percent initial measurement
            def scaling(ratios): return 100 * (numpy.ones(ratios.shape) - ratios)
            units = {'units': '% degradation / year', 'description': 'interpolatetd irradiance change'}
            formula.formulate('initial_irradiance_ratios', scaling, 'percent_irradiance_change', None, units)

            # get final irradiance ratio slice
            def finalizing(irradiances): return irradiances[-1]
            formula.formulate('initial_irradiance_ratios', finalizing, 'final_irradiance_ratios')

            # perform cascade to calculate all new features
            features = self.cascade(formula)

            # update route with Categories designation and link designation
            categories = self.apply(lambda feature: 'Categories' in feature.slash, features=features)
            [feature.update({'link': True}) for feature in categories]

            # create destination name and stash features
            now = self._note()
            destination = '{}/interpolations/OMI_Interpolations_{}_{}_{}.h5'.format(self.sink, band, diffuser, now)
            self._stash(features, destination, 'Data')

            # timestamp
            self._stamp('interpolated.')

        return None

    def chronicle(self, paths=None):
        """Chronicle the record of irradiance calibrations using collection 3 .met files.

        Arguments:
            paths: list of str, metadta pathnames

        Returns:
            None
        """

        # default paths to self
        paths = paths or [path for path in self._see(self.source) if '.met' in path]

        # open chronology
        chronology = self._load('chronology.json')

        # for each path
        for path in paths:

            # find the orbit number
            orbit = self._stage(path)['orbit']
            if orbit not in chronology.keys():

                # open as a text
                self._print('chronicling {}...'.format(path))
                text = self._know(path)

                # find configuration ids
                lines = [(index, line) for index, line in enumerate(text) if 'InstrumentConfigurationIDs' in line]
                index = lines[0][0]

                # find the following value
                information = [line for line in text[index + 1: index + 20] if ' VALUE' in line][0]
                self._print(information)

                # try to
                try:

                    # translate into ids
                    ids = [int(member) for member in ast.literal_eval(information.split('=')[-1].strip())]

                # unless it hits an eof
                except SyntaxError:

                    # add additional parenthesis and continue as before
                    information = information.strip('"').strip().strip(',') + ')'
                    ids = [int(member) for member in ast.literal_eval(information.split('=')[-1].strip())]

                # add ids to chronology
                chronology[orbit] = ids

        # write chronology
        self._dump(chronology, 'chronology.json')

        return None

    def degrade(self):
        """Calculate diffuser degradation rates from drifts in aggrergate rawflux files.

        Arguments:
            None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/degradations'.format(self.sink))

        # get all regression files
        directory = '{}/regressions'.format(self.sink)
        paths = self._see(directory)

        # group paths by band
        bands = self._group(paths, lambda path: re.search('BAND[1-3]', path).group())
        for band, members in bands.items():

            # timestamp
            self._stamp('solving for degradation rates for {}...'.format(band), initial=True)

            # empty old features, then ingest all paths
            self._depopulate()
            [self.ingest(path, discard=False) for path in members]

            # set individual diffuser names
            quartz = self.diffusers[8]
            aluminum = self.diffusers[18]
            backup = self.diffusers[30]
            detector = 'non_diffuser_components'

            # make address
            address = 'Categories/Degradations'

            # begin formula
            formula = Formula()

            # include wavelength grid and pixel grid
            formula.formulate('wavelength_grid')

            # calculate the ratio of exposure times for the two aluminum diffusers: x = (nb * xb) / (na * xa)
            def rationalizing(exposures, exposuresii): return numpy.array([exposuresii.sum() / exposures.sum()])
            parameters = ['{}/total_exposure'.format(diffuser) for diffuser in (aluminum, backup)]
            formula.formulate(parameters, rationalizing, 'aluminum_exposures_ratio', address)

            # compare hourly degradation rates
            def comparing(quartzes, aluminums): return -(quartzes - aluminums)
            parameters = ['{}/hourly_percent_change'.format(diffuser) for diffuser in (quartz, aluminum)]
            formula.formulate(parameters, comparing, 'quartz_regular_comparison', address)

            # compare hourly degradation rates
            def comparing(quartzes, backups): return -(quartzes - backups)
            parameters = ['{}/hourly_percent_change'.format(diffuser) for diffuser in (quartz, backup)]
            formula.formulate(parameters, comparing, 'quartz_backup_comparison', address)

            # set up tag specifics
            tags = {'rate': 'spectral_polynomials', 'linear': 'linear_slopes'}

            # go through each tag
            for tag, source in tags.items():

                # calculate the degradation of aluminum diffuser: rail = (pail - pbil) / (1 - x)
                def degrading(aluminums, backups, ratio): return (aluminums - backups) * (1 / (1.00001 - ratio[0]))
                parameters = ['{}/{}'.format(diffuser, source) for diffuser in (aluminum, backup)]
                parameters += ['aluminum_exposures_ratio']
                formula.formulate(parameters, degrading, '{}_degradation_{}'.format(aluminum, tag), address)

                # calculate the degradation of the backup aluminum diffuser: rbil = x * rail
                def degrading(ratio, aluminums): return ratio[0] * aluminums
                parameters = ['aluminum_exposures_ratio', '{}_degradation_{}'.format(aluminum, tag)]
                formula.formulate(parameters, degrading, '{}_degradation_{}'.format(backup, tag), address)

                # calculate the degradation of the rest of the detector: rdil = pbil - rbil
                def degrading(backups, degradations): return backups - degradations
                parameters = ['{}/{}'.format(backup, source), '{}_degradation_{}'.format(backup, tag)]
                formula.formulate(parameters, degrading, '{}_degradation_{}'.format(detector, tag), address)

                # calculate the degradation of the quartz volume diffuser: rqil = pqil - rdil
                def degrading(quartzes, degradations): return quartzes - degradations
                parameters = ['{}/{}'.format(quartz, source), '{}_degradation_{}'.format(detector, tag)]
                formula.formulate(parameters, degrading, '{}_degradation_{}'.format(quartz, tag), address)

                # alternatively, as per Schenkeveld et al: rbil ~ 0, so rdil = pbil
                parameters = ['{}/{}'.format(backup, source)]
                formula.formulate(parameters, None, '{}_degradation_{}x'.format(detector, tag), address)

                # the aluminum degradation rate in this case: rail = pail - rdil
                def approximating(aluminums, detectors): return aluminums - detectors
                parameters = ['{}/{}'.format(aluminum, source), '{}_degradation_{}x'.format(detector, tag)]
                formula.formulate(parameters, approximating, '{}_degradation_{}x'.format(aluminum, tag), address)

                # the quartz volume rate in this case: rqil = pqil - rdil
                def approximating(quartzes, detectors): return quartzes - detectors
                parameters = ['{}/{}'.format(quartz, source), '{}_degradation_{}x'.format(detector, tag)]
                formula.formulate(parameters, approximating, '{}_degradation_{}x'.format(quartz, tag), address)

                # finally, the backup rate can be adjusted based on the exposure ratio: rbil = x * rail
                def approximating(aluminums, ratio): return aluminums * ratio[0]
                parameters = ['{}_degradation_{}x'.format(aluminum, tag), 'aluminum_exposures_ratio']
                formula.formulate(parameters, approximating, '{}_degradation_{}x'.format(backup, tag), address)

            # perform cascade to calculate all new features
            features = self.cascade(formula)

            # update route with Categories designation and link designation
            categories = self.apply(lambda feature: 'Categories' in feature.slash, features=features)
            [feature.update({'link': True}) for feature in categories]

            # create destination name and stash featues
            now = self._note()
            destination = '{}/degradations/OMI_Degradations_{}_{}.h5'.format(self.sink, band, now)
            self._stash(features, destination, 'Data')

            # timestamp
            self._stamp('solved.')

        return None

    def diffuse(self):
        """Perform linear and polynomial fitting for diffuser rates of change.

        Arguments:
            None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/regressions'.format(self.sink))

        # get all aggregate files
        directory = '{}/interpolations'.format(self.sink)
        paths = self._see(directory)

        # define diffuser uses / year
        usages = {self.diffusers[8]: 365, self.diffusers[18]: 52, self.diffusers[30]: 12}

        # go through each path
        for path in paths:

            # ingest the path
            self.ingest(path)

            # extract band and diffuser names
            band = re.search('BAND[1-3]', path).group()
            diffuser = re.search('|'.join(self.diffusers.values()), path).group()

            # timestamp
            self._stamp('\nperforming regressions for {}, {}...'.format(diffuser, band), initial=True)

            # begin formula
            formula = Formula()

            # add independent variables
            independents = self.apply(lambda feature: 'IndependentVariables' in feature.slash)
            [formula.formulate(feature.name) for feature in independents]

            # define address
            address = 'Categories/{}/{}'.format(band, diffuser)

            # create beginning, ending time bracket
            def bracketing(fractions): return numpy.array([fractions[0], fractions[-1]])
            formula.formulate('orbit_start_year_fraction', bracketing, 'bracket_year_fraction', 'IndependentVariables')

            # copy total exposure
            formula.formulate('total_exposure', None, None, address)

            # sum exposures and translate from seconds to hours
            def summing(exposures): return numpy.array([sum(exposures) * (1 / 3600)])
            units = {'units': 'hours'}
            formula.formulate('total_exposure', summing, 'total_exposure_hours', address, units)

            # translate year fractions into hours of exposure
            def clocking(fractions): return (fractions - fractions[0]) * usages[diffuser] * (84 / 3600)
            units = {'units': 'hours'}
            formula.formulate('orbit_start_year_fraction', clocking, 'orbit_start_exposure_hours', None, units)

            # perform linear regressions at all wavelengths
            def raining(percents, times): return self._rain(percents, times)
            parameters = ['initial_irradiance_ratios', 'orbit_start_year_fraction']
            names = ['linear_slopes', 'linear_intercepts', 'linear_scores']
            units = [{'units': '% irradiance / yr'}, None, {'units': '_'}]
            formula.formulate(parameters, raining, names, address, units)

            # perform linear regressions at all wavelengths / hourly exposure
            def raining(percents, times): return self._rain(percents, times)
            parameters = ['initial_irradiance_ratios', 'orbit_start_exposure_hours']
            names = ['exposure_slopes', 'exposure_intercepts', 'exposure_scores']
            units = [{'units': '% irradiance / hr'}, None, {'units': '_'}]
            formula.formulate(parameters, raining, names, address, units)

            # reconstruct trends
            def trending(intercepts, slopes, times): return numpy.array([intercepts + slopes * times[0], intercepts + slopes * times[1]])
            parameters = ['linear_intercepts', 'linear_slopes', 'bracket_year_fraction']
            formula.formulate(parameters, trending, 'linear_trendlines')

            # perform polynomial regression at all rows
            def winding(slopes, grid): return self._wind(slopes, grid)
            parameters = ['linear_slopes', 'wavelength_grid']
            names = ['spectral_polynomials', 'spectral_coefficients']
            units = [{'units': '% irradiance / yr'}, None, {'units': '_'}]
            formula.formulate(parameters, winding, names, address, units)

            # calculate hourly exposure rate
            #def clocking(slopes, bracket): return slopes * (bracket[-1] - bracket[0]) * 3600 / (77 * usages[diffuser])
            def clocking(slopes, bracket): return slopes * 3600 / (84 * usages[diffuser])
            parameters = ['linear_slopes', 'bracket_year_fraction']
            units = {'units': '% irradiance change / hour'}
            formula.formulate(parameters, clocking, 'hourly_percent_change', address, units)

            # perform cascade to calculate all new features
            features = self.cascade(formula)

            # update route with Categories designation and link designation
            categories = self.apply(lambda feature: 'Categories' in feature.slash, features=features)
            [feature.update({'link': True}) for feature in categories]

            # create destination name and stash featues
            now = self._note()
            destination = '{}/regressions/OMI_Regressions_{}_{}_{}.h5'.format(self.sink, band, diffuser, now)
            self._stash(features, destination, 'Data')

            # timestamp
            self._stamp('regressed.')

        return None

    def flare(self, paths, old, middle, new, collection):
        """Combine all bands into single files for diffusers.

        Arguments:
            paths: list of str
            old: str
            middle: str
            new: str

        Returns:
            None
        """

        # get bands based on collection
        threes = {'band1': 'UV-1 Swath', 'band2': 'UV-2 Swath', 'band3': 'VIS Swath'}
        fours = {'band1': 'BAND1_IRRADIANCE', 'band2': 'BAND2_IRRADIANCE', 'band3': 'BAND3_IRRADIANCE'}
        bands = {'3': threes, '4': fours}

        # make the new directories
        self._make('{}/{}'.format(self.sink, middle))
        self._make('{}/{}'.format(self.sink, new))

        # groups files by diffusers
        diffusers = self._group(paths, lambda path: re.search('|'.join(self.diffusers.values()), path).group())
        for diffuser, members in diffusers.items():

            self._tell(members)

            # for each member
            for member in members:

                # get the collections
                band = re.search('BAND[1-3]', member).group().lower()

                # ingest the file
                self.ingest(member)

                # get all features that are band centric
                features = self.dig('Categories/{}'.format(bands[collection][band]))

                self._print('features: {}'.format(len(features)))

                # get all feature names
                names = [feature.name for feature in features]
                aliases = ['{}_{}'.format(band, name) for name in names]

                # make mimic
                self.mimic([member], old, middle, names, aliases)

            # staple together
            entries = [entry for entry in self._see('{}/{}'.format(self.sink, middle)) if diffuser in entry]
            formats = (self.sink, new, collection, diffuser, self._note())
            destination = '{}/{}/OMI_Rawflux_v0{}_2004_2021_{}_{}.h5'.format(*formats)
            self.staple(entries, destination)

        return None

    def fuse(self):
        """Fuse all individual trending files into larger aggregates.

        Arguments:
            None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/fusions'.format(self.sink))

        # get all rawflux paths
        gazes = self._see('{}/gazes'.format(self.sink))

        # sort into diffusers
        diffusers = self._group(gazes, lambda path: re.search('|'.join(self.diffusers.values()), path).group())

        # for each diffuser
        for diffuser, members in diffusers.items():

            # sort into band based on paths
            bands = self._group(members, lambda path: re.search('BAND[1-3]', path).group())

            # go through each band
            for band, paths in bands.items():

                # print status
                print('\nfusing {} rawgazes for {}, {}...'.format(len(paths), band, diffuser))

                # search for fusion files for the band
                fusions = [path for path in self._see('{}/fusions'.format(self.sink)) if band in path]
                fusions = [path for path in fusions if diffuser in path]

                # create destination path
                now = self._note()
                destination = '{}/fusions/OMI_Rawgazes_{}_{}_{}.h5'.format(self.sink, band, diffuser, now)

                # meld
                self.meld(paths, fusions, destination)

            # timestamp
            self._stamp('fused.')

        return None

    def gaze(self, paths=None):
        """Feed one orbit at a time through diagnosis.

        Arguments:
            paths = None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/gazes'.format(self.sink))

        # get default paths
        paths = paths or self.paths

        # go through each path
        for path in paths:

            # process the orbit
            self.scrutinize(path)

        return None

    def grill(self, paths, bands, reservoir, collection):
        """Calculate the binned wavelength grids and ratios.

        Arguments:
            None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/grids'.format(self.sink))

        # go through each path
        for path in paths:

            # extract band and diffuser names
            diffuser = re.search('|'.join(self.diffusers.values()), path).group()

            # timestamp
            self._stamp('\nperforming interpolations for {}...'.format(diffuser), initial=True)

            # ingest the path
            self.ingest(path)

            # begin formulas
            formula = Formula()

            # add independent variables
            independents = self.apply(lambda feature: 'IndependentVariables' in feature.slash)
            [formula.formulate(feature.name) for feature in independents]

            # go through bands
            for band in bands:

                # # calculate total exposure times
                # def exposing(exposures, times): return exposures * times
                # formula.formulate(['exposure_time_index_zero', 'OrbitTrackLength'], exposing, 'total_exposure')

                # determine number of spectral pixels and calculate the wavelengths
                def calculating(coefficient, column, pixels): return self._calculate(coefficient, column, pixels.shape[-1])
                parameters = ['{}_wavelength_coefficient_elevation_zero'.format(band)]
                parameters += ['{}_wavelength_reference_column'.format(band)]
                parameters += ['{}_{}'.format(band,  reservoir)]
                name = '{}_calculated_wavelengths'.format(band)
                units = {'units': 'nm', 'description': 'calculated wavelengths'}
                formula.formulate(parameters, calculating, name, None, units)

                # add pixel grid
                def pixelating(wavelengths): return numpy.round(wavelengths.mean(axis=(0, 1)), 1)
                units = {'units': 'nm', 'description': 'column averaged calculated wavelengths'}
                parameter = '{}_calculated_wavelengths'.format(band)
                name = '{}_pixel_grid'.format(band)
                formula.formulate(parameter, pixelating, name, 'IndependentVariables', units)

                # construct the wavelength grid and interpolate the solar irradiances
                def interpolating(irradiances, wavelengths): return self._interpolate(irradiances, wavelengths, band.upper())
                parameters = ['{}_{}'.format(band, reservoir)]
                parameters += ['{}_calculated_wavelengths'.format(band)]
                names = ['{}_interpolated_irradiances'.format(band), '{}_wavelength_grid'.format(band)]
                addresses = [None, 'IndependentVariables']
                attributes = [{'units': 'irradiance', 'description': 'linearly interpolated irradiances'}, {'units': 'nm'}]
                formula.formulate(parameters, interpolating, names, addresses, attributes)

                # add formula for irradiance ratio
                def dividing(tensor): return tensor / tensor[100]
                parameter = '{}_interpolated_irradiances'.format(band)
                name = '{}_initial_irradiance_ratios'.format(band)
                units = {'units': 'irradiance ratio', 'description': 'interpolated irradiances compared to initial irradiance'}
                formula.formulate(parameter, dividing, name, None, units)
                # #
                # # add formula for irradiance ratio
                # def dividing(tensor): return tensor / tensor[0]
                # units = {'units': 'irradiance ratio', 'description': 'irradiances compared to initial irradiance'}
                # formula.formulate('irradiance_after_relirr_avg', dividing, 'initial_uninterpolated_ratios', None, units)
                #
                # # scale interpolations to initial measurement for percent initial measurement
                # def scaling(ratios): return 100 * (numpy.ones(ratios.shape) - ratios)
                # units = {'units': '% degradation / year', 'description': 'interpolatetd irradiance change'}
                # formula.formulate('initial_irradiance_ratios', scaling, 'percent_irradiance_change', None, units)
                #
                # # get final irradiance ratio slice
                # def finalizing(irradiances): return irradiances[-1]
                # formula.formulate('initial_irradiance_ratios', finalizing, 'final_irradiance_ratios')

            # perform cascade to calculate all new features
            features = self.cascade(formula)

            # update route with Categories designation and link designation
            categories = self.apply(lambda feature: 'Categories' in feature.slash, features=features)
            [feature.update({'link': True}) for feature in categories]

            # create destination name and stash features
            now = self._note()
            destination = '{}/grids/OMI_Grid_v0{}_{}_{}.h5'.format(self.sink, collection, reservoir, diffuser, now)
            self._stash(features, destination, 'Data')

            # timestamp
            self._stamp('grilled.')

        return None

    def inspect(self, band, diffuser, row, *wavelengths):
        """Inspect the degradation regression results for a paricular time series.

        Arguments:
            band: int, band number
            diffuser: int, diffuser code
            rows: int, row number
            *wavelengths: unpacked list of floats, the wavelength grid points

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/inspections'.format(self.sink))

        # get all paths in the degradations
        paths = self._see('{}/degradations'.format(self.sink))

        # get the diffuser description
        description = self.diffusers[diffuser]

        # get the path with the right band and diffuser
        path = [path for path in paths if 'BAND' + str(band) in path and description in path][0]

        # fetch the hdf5 file
        five = self._fetch(path)

        # get time data
        times = [float(entry) for entry in five['IndependentVariables']['OrbitStartTimeFrYr'][:]]
        beginning = times[0]
        ending = times[-1]

        # grab the irradiance data
        irradiances = five['Data']['percent_irradiance_change'][:]
        grid = five['Data']['wavelength_grid'][:]

        # grab the regression data
        scores = five['Data']['r2_scores'][:]
        slopes = five['Data']['slopes'][:]
        intercepts = five['Data']['intercepts'][:]

        # close the hdf5 file
        five.close()

        # define plotting colors
        colors = ['r', 'g', 'b', 'm'] * len(wavelengths)

        # for each wavelength
        lines = []
        for wavelength, color in zip(wavelengths, colors):

            # get the grid index
            wave = list(grid).index(wavelength)

            # get the interpolated irradiances
            irradiance = [float(image[row][wave]) for image in irradiances]
            slope = float(slopes[row][wave])
            intercept = float(intercepts[row][wave])

            # get r^2 score
            score = round(scores[row][wave], 2)

            # construct line
            line = (times, irradiance, color + '-', '{} ({})'.format(wavelength, score))
            lines.append(line)

            # construct regression line
            abscissa = [beginning, ending]
            ordinate = [intercept + slope * beginning, intercept + slope * ending]
            regression = (abscissa, ordinate, color + '--', None)
            lines.append(regression)

        # construct title and labels
        title = 'band {}, diffuser {}, row {}'.format(band, diffuser, row)
        independent = 'year'
        dependent = 'irradiance (% from initial)'
        text = [title, independent, dependent]

        # construct destination
        formats = (self.sink, band, diffuser, row, tuple(wavelengths))
        destination = '{}/inspections/inspection_band{}_diffuser{}_row{}_waves{}.png'.format(*formats)

        # draw the graph
        self._draw(lines, text, destination)

        return None

    def observe(self, paths=None, tag='meld', middle='months', folder='observations'):
        """Weld all single orbit fluxes into fusion files.

        Arguments:
            paths: list of str, the file paths, defaults to self.paths
            tag: str, added tag
            middle: str, folder for intermediates
            folder: str, folder name

        Returns:
            None
        """

        # default paths to self.paths
        paths = paths or self.paths

        # make middle and endpoint folders
        self._make('{}/{}'.format(self.sink, middle))
        self._make('{}/{}'.format(self.sink, folder))

        # get current time
        now = self._note()

        # sort entries by orbits
        recents = []
        orbits = self._group(paths, lambda path: self._stage(path)['orbit'])
        for orbit, stuff in orbits.items():

            # sort by version
            stuff.sort(key=lambda path: self._stage(path)['version'])
            recents.append(stuff[-1])

        # sort recents by year and month
        months = self._group(recents, lambda path: self._stage(path)['date'][:7])
        for month, series in months.items():

            # sort entries by orbit number
            series.sort(key=lambda entry: int(self._stage(entry)['orbit']))

            self._stamp('{} entries...'.format(len(paths)))

            # create destination name
            destination = '{}/{}/OMI_Rawgazes_{}.h5'.format(self.sink, middle, month)

            # merge
            self._stamp('merging for {}'.format(destination))
            self.merge(series, destination)

            self._stamp('merged.')

        # gather all paths from middle folder with same band and diffuser
        entities = [path for path in self._see('{}/{}'.format(self.sink, middle))]
        entities.sort()

        # get destination name
        destination = '{}/{}/OMI_Observations_{}_{}.h5'.format(self.sink, folder, tag, now)

        # merge
        self._stamp('merging for {}'.format(destination))
        self.merge(entities, destination)

        self._stamp('merged.')

        return None

    def recognize(self, path):
        """Determine if the ic id in the orbit is recognized as a diffuser calibration.

        Arguments:
            path: str, file path

        Returns:
            boolean, orbit recognized?
        """

        # print status
        print('inspecting {} for ic id...'.format(path))

        # assume the orbit is not recognized
        recognition = ''

        # check for collection
        collection = self._stage(path)['collection']

        # if the collection is 4
        if collection == '4':

            # try to
            code = 0
            try:

                # open the hdf5 file
                five = self._fetch(path)

                # construct route, assuming a certain structure
                band = 'BAND1_IRRADIANCE'
                mode = list(five[band].keys())[0]
                instrument = 'INSTRUMENT'
                configuration = 'instrument_configuration'

                # get the ic id code
                code = five[band][mode][instrument][configuration][:][0][0][0]

            # unless there is a missing key
            except KeyError:

                # in which case, it is not recoginized
                print('file structure not recognized')

            # regardless
            finally:

                # close file
                five.close()

        # otherwise assume collection 3
        else:

            # try to
            code = 0
            try:

                # scrounge the code from hdf4 without converting
                code = self._scrounge(path, ['Avg Sun', 'Data Fields', 'InstrumentConfigurationId'], [0])

            # unless there is a missing key
            except KeyError:

                # in which case, it is not recoginized
                print('file structure not recognized')

        # if the code is in the diffuser set
        if code in self.diffusers.keys():

            # file is recognized
            recognition = self.diffusers[code]

        # otherwise
        else:

            # print status
            print('ic id {} not recognized'.format(code))

        return recognition

    def reflect(self, paths=None):
        """Reflect converted collection3 solar data into rawflux files.

        Arguments:
            paths: list of str, filepaths, source directory by default

        Returns:
            None
        """

        # make gazes directory
        self._make('{}/gazes'.format(self.sink))

        # set up bands
        bands = {'BAND1': 'Avg Sun Volume UV-1 Swath (30x159x8)'}
        bands.update({'BAND2': 'Avg Sun Volume UV-2 Swath (60x557x8)'})
        bands.update({'BAND3': 'Avg Sun Volume VIS Swath (60x751x8)'})

        # set diffuser to volume for now
        diffuser = self.diffusers[8]

        # get paths
        paths = paths or self.paths

        # for each path
        for path in paths:

            # ingest path
            self.ingest(path)

            # go through bands
            for band, field in bands.items():

                # print status
                self._stamp('subsetting {}...'.format(band), initial=True)

                # grab the independent variables from file path
                independents = self._parse(path)

                # create features from all independent variables
                independents = {field: numpy.array([quantity]) for field, quantity in independents.items()}
                independents = [Feature(['IndependentVariables', name], array) for name, array in independents.items()]

                # subset features based on band
                subset = self.dig(field)

                # begin formula
                formula = Formula()

                # copy over some parameters unchanged, but removing trivial dimensions
                def squeezing(tensor): return numpy.array([tensor.squeeze()])
                formula.formulate('Geolocation Fields/Time', squeezing, 'time')
                formula.formulate('WavelengthReferenceColumn', squeezing, 'wavelength_reference_column')

                # compose irradiance data from manitssa and exponent
                parameters = ['IrradianceMantissa', 'IrradianceExponent']
                def composing(mantissas, exponents): return self._compose(mantissas, exponents)
                formula.formulate(parameters, composing, 'irradiance_raw')

                # transfer earth sun distance to independent variables, converting to ratio
                def converting(distances): return distances * (1 / 1.49597870e11)
                address = 'IndependentVariables'
                units = {'units': 'astronomical units'}
                formula.formulate('EarthSunDistance', converting, 'OrbitSunEarthDistance', address, units)

                # adjust irradiance units for earth sun distance, and consider analogous to Col4 avg irradiance
                def adjusting(irradiances, distances): return self._adjust(irradiances, distances)
                parameters = ['irradiance_raw', 'OrbitSunEarthDistance']
                formula.formulate(parameters, adjusting, 'irradiance_after_relirr_avg')

                # make up processor information, using time as dummy
                def processing(tensor): return numpy.array(['_']).astype('S')
                formula.formulate('time', processing, 'processor_version')

                # get processor core information, using time as dummy
                def coring(tensor): return numpy.array(['_']).astype('S')
                formula.formulate('time', coring, 'core_processor_version')

                # get first member of exposure time
                def selecting(tensor): return numpy.array([tensor.squeeze()])
                formula.formulate('ExposureTime', selecting, 'exposure_time_index_zero')

                # determine track length from longitude path
                def tracking(tensor): return numpy.array([tensor.shape[1]]).astype('int')
                units = {'units': 'images'}
                formula.formulate('SpacecraftLongitude', tracking, 'OrbitTrackLength', address, units)

                # extract first longitude
                def extracting(tensor): return tensor.take(0, axis=1)
                formula.formulate('SpacecraftLongitude', extracting, 'OrbitLongitude', address)

                # entire elevation index no given, so fake it at zero
                def indexing(tensor): return numpy.array([0]).astype(int)
                formula.formulate('SolarElevationMinimum', indexing, 'solar_elevation_index')

                # entire elevation index not given, so fake it at zero
                def elevating(angles): return numpy.array([0.0])
                formula.formulate('SolarElevationMinimum', elevating, 'solar_elevation_angle')

                # assume quartz volume
                def identifying(tensor): return numpy.array([8])
                formula.formulate('NumTimes', identifying, 'ic_id')

                # average the pixel quality
                def qualifying(tensor): return tensor.squeeze()
                formula.formulate('PixelQualityFlags', qualifying, 'irradiance_avg_spectral_channel_quality')

                # find wavelength coefficients at that index
                def waving(coefficients): return coefficients.squeeze()
                name = 'wavelength_coefficient_elevation_zero'
                formula.formulate('WavelengthCoefficient', waving, name)

                # perform cascade to calculate all new features
                calculations = self.cascade(formula, subset)

                # update route with Categories designation and link designation
                categories = self.apply(lambda feature: 'IndependentVariables' not in feature.slash, features=calculations)
                [feature.divert('Categories') for feature in categories]
                [feature.update({'link': True}) for feature in categories]

                # create destination name
                now = self._note()
                stage = self._stage(path)
                orbit, date = stage['orbit'], stage['date']
                formats = (self.sink, band, diffuser, date, orbit, now)
                destination = '{}/gazes/OMI_Rawflux_{}_{}_{}-o{}_{}.h5'.format(*formats)

                # stash features as new hdf5 file
                self._stash(calculations + independents, destination, 'Data')

        return None

    def reproduce(self):
        """Reproduce select figures from the Schenkeveld paper.

        Arguments:
            None

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/reproductions'.format(self.sink))

        # start plots
        self._stamp('plotting...', initial=True)

        # collect all paths
        fusions = self._see('{}/fusions'.format(self.sink))

        # group by bands
        bands = self._group(fusions, lambda path: re.search('BAND[1-3]', path).group())

        # go through each path
        for band, paths in bands.items():

            # sort paths
            paths.sort()

            # begin irradiances etc collection
            irradiances = []
            identifiers = []
            times = []

            # go through paths
            for path in paths:

                # open the hdf5 file
                five = self._fetch(path)

                # get irradiance data
                irradiance = five['Data']['irradiance_after_relirr_avg'][:]
                irradiances.append(irradiance)

                # get ic id data
                identifier = five['Data']['ic_id'][:]
                identifiers.append(identifier)

                # get times info
                time = five['IndependentVariables']['OrbitStartTimeFrYr'][:]
                times.append(time)

                # close file
                five.close()

            # perform concatenation
            irradiances = numpy.concatenate(irradiances, axis=0)
            identifiers = numpy.concatenate(identifiers, axis=0).astype(int)
            times = numpy.concatenate(times, axis=0)

            print('irradiances: {}'.format(irradiances.shape))
            print('identifiers: {}'.format(identifiers.shape))
            print('times: {}'.format(times.shape))

            # create texts
            title = 'Relative Signal {}\na la Schenkeveld Figure 26'.format(band)
            independent = 'Year'
            dependent = 'Ratio [-]'
            texts = [title, independent, dependent]

            # begin plot lines
            lines = []

            # set colors and labels according to Schenkeveld figure
            colors = {8: 'b-', 18: 'g-', 30: 'r-'}
            labels = {8: 'QVD', 18: 'Regular Al', 30: 'Backup Al'}

            # for each diffuser
            for code, description in list(self.diffusers.items()):

                # get indices
                indicies = numpy.where(identifiers == code)[0]

                # cut off spectral smile area and subset to diffuser
                subset = irradiances[indicies, :, 20:140]

                # get time data for diffuser
                temporals = times[indicies]

                # calculate ratios, then average them
                initials = numpy.array([subset[0]] * len(subset))
                ratios = subset / initials
                ratios = numpy.average(ratios, axis=(1, 2))

                # # average first, then calculate ratios
                # averages = numpy.average(irradiances, axis=(1, 2))
                # ratios = averages / averages[0]

                # construct line
                line = (temporals, ratios, colors[code], labels[code])
                lines.append(line)

            # add destinaton
            destination = '{}/reproductions/Schenkeveld_26_{}.png'.format(self.sink, band)

            # draw plot
            self._draw(lines, texts, destination)

        # timestamp
        self._stamp('plotted.')

        return None

    def reradiate(self, path, diffuser):
        """Convert converted collection3 solar data into rawflux files.

        Arguments:
            path: collection 3 file path
            diffuser: str, diffuser name

        Returns:
            None
        """

        # make gazes directory
        self._make('{}/gazes'.format(self.sink))

        # convert to hdf5
        folder = '{}/tmp'.format(self.sink)
        self.ingest(path, folder=folder)

        # set up bands
        bands = {'BAND1': 'Avg Sun Volume UV-1 Swath (30x159x8)'}
        bands.update({'BAND2': 'Avg Sun Volume UV-2 Swath (60x557x8)'})
        bands.update({'BAND3': 'Avg Sun Volume VIS Swath (60x751x8)'})

        # go through bands
        for band, field in bands.items():

            # print status
            self._stamp('subsetting {}...'.format(band), initial=True)

            # grab the independent variables from file path
            independents = self._parse(path)

            # create features from all independent variables
            independents = {field: numpy.array([quantity]) for field, quantity in independents.items()}
            independents = [Feature(['IndependentVariables', name], array) for name, array in independents.items()]

            # subset features based on band
            subset = self.dig(field)

            # begin formula
            formula = Formula()

            # copy over some parameters unchanged, but removing trivial dimensions
            def squeezing(tensor): return numpy.array([tensor.squeeze()])
            formula.formulate('WavelengthReferenceColumn', squeezing, 'wavelength_reference_column')
            formula.formulate('Time', squeezing, 'time')

            # compose irradiance data from manitssa and exponent
            parameters = ['IrradianceMantissa', 'IrradianceExponent']
            def composing(mantissas, exponents): return numpy.array([self._compose(mantissas, exponents)])
            formula.formulate(parameters, composing, 'irradiance_raw')

            # transfer earth sun distance to independent variables, converting to ratio
            def converting(distances): return distances[0] * (1 / 1.49597870e11)
            address = 'IndependentVariables'
            units = {'units': 'astronomical units'}
            formula.formulate('EarthSunDistance', converting, 'orbit_earth_sun_distance', address, units)

            # adjust irradiance units for earth sun distance, and consider analogous to Col4 avg irradiance
            def adjusting(irradiances, distances): return self._adjust(irradiances, distances)
            parameters = ['irradiance_raw', 'orbit_earth_sun_distance']
            formula.formulate(parameters, adjusting, 'irradiance_avg')

            # make up processor information, using time as dummy
            def processing(tensor): return numpy.array(['_']).astype('S')
            formula.formulate('time', processing, 'processor_version')

            # get processor core information, using time as dummy
            def coring(tensor): return numpy.array(['_']).astype('S')
            formula.formulate('time', coring, 'core_processor_version')

            # get first member of exposure time
            def selecting(tensor): return numpy.array([tensor.squeeze()])
            formula.formulate('ExposureTime', selecting, 'exposure_time_index_zero')

            # determine track length from longitude path
            def tracking(tensor): return numpy.array([tensor.shape[1]]).astype('int')
            units = {'units': 'images'}
            formula.formulate('SpacecraftLongitude', tracking, 'orbit_track_length', address, units)

            # extract first longitude
            def extracting(tensor): return tensor.take(0, axis=1)
            formula.formulate('SpacecraftLongitude', extracting, 'orbit_longitude', address)

            # entire elevation index no given, so fake it at zero
            def indexing(tensor): return numpy.array([0]).astype(int)
            formula.formulate('SolarElevationMinimum', indexing, 'solar_elevation_index')

            # entire elevation index not given, so fake it at zero
            def elevating(angles): return numpy.array([0.0])
            formula.formulate('SolarElevationMinimum', elevating, 'solar_elevation_angle')

            # assume quartz volume
            def identifying(tensor): return numpy.array([8])
            formula.formulate('NumTimes', identifying, 'ic_id')

            # average the pixel quality
            def qualifying(tensor): return tensor[0]
            formula.formulate('PixelQualityFlags', qualifying, 'irradiance_avg_spectral_channel_quality')

            # find wavelength coefficients at that index
            def waving(coefficients): return coefficients[0]
            name = 'wavelength_coefficient_elevation_zero'
            formula.formulate('WavelengthCoefficient', waving, name)

            # perform cascade to calculate all new features
            calculations = self.cascade(formula, subset)

            # update route with Categories designation and link designation
            categories = self.apply(lambda feature: 'IndependentVariables' not in feature.slash, features=calculations)
            [feature.divert('Categories') for feature in categories]
            [feature.update({'link': True}) for feature in categories]

            # create destination name
            now = self._note()
            stage = self._stage(path)
            orbit, date = stage['orbit'], stage['date']
            formats = (self.sink, band, diffuser, date, orbit, now)
            destination = '{}/gazes/OMI_Rawflux_{}_{}_{}-o{}_{}.h5'.format(*formats)

            # stash features as new hdf5 file
            self._stash(calculations + independents, destination, 'Data')

        return None

    def scrutinize(self, path):
        """Subset an individual cal file into a calibration record.

        Arguments:
            path: str, filepath

        Returns:
            None
        """

        # ingest the path
        self._stamp('ingesting {}...'.format(path), initial=True)
        self.ingest(path)

        # begin feature collecting formulas
        formula = Formula()

        # grab the independent variables from file path
        independents = self._parse(path)
        independents = {field: numpy.array([quantity]) for field, quantity in independents.items()}
        independents = [Feature(['IndependentVariables', name], array) for name, array in independents.items()]
        features = independents

        # get all processor features
        processor = self.dig('PROCESSOR')

        # get processor information
        def processing(tensor): return numpy.array([self._process(tensor[0])['processor']]).astype('S')
        formula.formulate('job_configuration', processing, 'processor_version')

        # get processor core information
        def coring(tensor): return numpy.array([self._process(tensor[0])['core']]).astype('S')
        formula.formulate('job_configuration', coring, 'core_processor_version')

        # group all feature by band
        subset = [feature for feature in self if 'DETECTOR' in feature.slash]
        detectors = self._group(subset, lambda feature: re.search('DETECTOR[1-2]', feature.slash).group())

        # go through bands
        for detector, members in detectors.items():

            # print status
            self._stamp('subsetting {}...'.format(detector))

            # get all signal averages
            signals = self.dig('signal_avg', members)
            for signal in signals:

                # add readings
                mode = re.search('MODE_[0-9]{3}', signal.slash)
                mode = mode.group() if mode else '_'
                formula.formulate(signal.slash, None, '{}_{}_signal_avg'.format(detector.lower(), mode.lower()))

            # get all temperatures
            temperatures = self.dig('temp', members)
            for temperature in temperatures:

                # add average readings
                def averaging(temperature): return numpy.array([temperature.mean()])
                formula.formulate(temperature.slash, averaging, '{}_avg'.format(temperature.name), 'Temperatures')

            # # copy over some parameters unchanged
            # formula.formulate('irradiance_after_relirr_avg')
            # formula.formulate('irradiance_avg')
            # formula.formulate('irradiance_avg_quality_level')
            # formula.formulate('irradiance_avg_spectral_channel_quality')
            # formula.formulate('OBSERVATIONS/time')
            # formula.formulate('wavelength_reference_column')
            #
            # # transfer earth sun distance to independent variables
            # address = 'IndependentVariables'
            # formula.formulate('GEODATA/earth_sun_distance', None, 'orbit_earth_sun_distance', address)
            #
            # # get first member of exposure time and temps
            # def selecting(tensor): return numpy.array([tensor.squeeze()[0]])
            # formula.formulate('exposure_time', selecting, 'exposure_time_index_zero')
            # formula.formulate('temp_opb', selecting, 'temp_opb_index_zero')
            # formula.formulate('temp_elu', selecting, 'temp_elu_index_zero')
            #
            # # determine track length from longitude path
            # def tracking(tensor): return numpy.array([tensor.shape[1]]).astype('int')
            # units = {'units': 'images'}
            # formula.formulate('satellite_longitude', tracking, 'orbit_track_length', address, units)
            #
            # # extract first longitude
            # def extracting(tensor): return tensor.take(0, axis=1)
            # formula.formulate('satellite_longitude', extracting, 'orbit_longitude', address)
            #
            # # find elevation index closest to zero
            # def indexing(tensor): return numpy.array([numpy.argsort(tensor ** 2)[0][0]]).astype(int)
            # formula.formulate('solar_elevation_angle', indexing, 'solar_elevation_index')
            #
            # # find elevation at that index
            # def elevating(angles, index): return numpy.array([angles.squeeze().take(int(index[0]), axis=0)])
            # formula.formulate(['solar_elevation_angle', 'solar_elevation_index'], elevating)
            #
            # # get ic_id from first entry
            # def identifying(tensor): return numpy.array([tensor.squeeze()[0].astype(int)])
            # formula.formulate('instrument_settings/ic_id', identifying)
            #
            # # find wavelength coefficients at that index
            # def waving(coefficients, index): return numpy.array([coefficients.squeeze().take(int(index[0]), axis=0)])
            # name = 'wavelength_coefficient_elevation_zero'
            # formula.formulate(['wavelength_coefficient', 'solar_elevation_index'], waving, name)

        # perform cascade to calculate all new features
        features += self.cascade(formula, subset + processor)

        # update route with Categories designation and link designation
        address = 'IndependentVariables'
        categories = self.apply(lambda feature: address not in feature.slash, features=features)
        [feature.divert('Categories') for feature in categories]
        [feature.update({'link': True}) for feature in categories]

        # create destination name and stash features
        now = self._note()
        stage = self._stage(path)
        orbit = stage['orbit']
        date = stage['date']
        destination = '{}/gazes/OMCALTRACK_{}-o{}_{}.h5'.format(self.sink, date, orbit, now)
        self._stash(features + independents, destination, 'Data')

        return None

    def sizzle(self):
        """Create specific probes for comparison with irradiance slides.

        Arguments:
            None

        Returns:
            None
        """

        # go through all bands
        bands = ['BAND{}'.format(number) for number in range(1, 4)]
        for band in bands:

            # grab all band1 files
            paths = [path for path in self.paths if band in path]

            # for each path
            monthlies = []
            for path in paths:

                # get the diffuser
                diffuser = re.search('|'.join(self.diffusers.values()), path).group()

                # ingest and get rid of Data links
                self.ingest(path)
                features = self.dig('Categories') + self.dig('IndependentVariables')

                # begin formula
                formula = Formula()

                # bin irradiances by month
                def calendarizing(irradiances, years, months): return self._calendarize(irradiances, years, months)
                parameters = ['interpolated_irradiances', 'orbit_start_yr', 'orbit_start_mon']
                names = ['month_binned_irradiances_{}'.format(diffuser), 'orbit_start_time_fr_yr_{}'.format(diffuser)]
                addresses = ['Categories', 'IndependentVariables']
                formula.formulate(parameters, calendarizing, names, addresses)

                # transfer wavelength grid
                formula.formulate('wavelength_grid')

                # make cascade
                cascade = self.cascade(formula, features, scan=True)
                monthlies += cascade

            # create diffuser ratios
            diffusers = self.diffusers
            formula = Formula()

            # synchronize all diffuser times
            def synchronizing(one, two, three): return self._synchronize(one, two, three)
            parameters = ['orbit_start_time_fr_yr_{}'.format(diffuser) for diffuser in diffusers.values()]
            formula.formulate(parameters, synchronizing, 'orbit_start_time_fr_yr_common')

            # go through code combinnations
            codex = [(8, 18), (8, 30), (18, 30)]
            for codes in codex:

                # make ratios of qvd to regular
                def rationalizing(one, two, times, timesii, common): return self._rationalize(one, two, times, timesii, common)
                parameters = ['month_binned_irradiances_{}'.format(diffusers[code]) for code in codes]
                parameters += ['orbit_start_time_fr_yr_{}'.format(diffusers[code]) for code in codes]
                parameters += ['orbit_start_time_fr_yr_common']
                name = 'monthly_ratios_{}_vs_{}'.format(*[diffusers[code] for code in codes])
                formula.formulate(parameters, rationalizing, name)

                # average across certain rows
                def clumping(irradiances): return irradiances[:, :, :].mean(axis=1)
                parameter = 'monthly_ratios_{}_vs_{}'.format(*[diffusers[code] for code in codes])
                name = parameter + '_select_rows'
                formula.formulate(parameter, clumping, name)

            # create comparison features
            comparison = self.cascade(formula, monthlies) + monthlies

            # stash features
            destination = '{}/{}_monthly_irradiances.h5'.format(self.source.replace('interpolations', 'months'), band)
            [feature.update({'link': True}) for feature in comparison if 'Categories' in feature.slash]
            self._stash(comparison, destination, 'Data')

        return None

    def trend(self, *rows):
        """Plot all diffuser degradation trends for the given rows.

        Arguments:
            *rows: unpacked list of ints, the particular rows to plot

        Returns:
            None
        """

        # create folder if not yet made
        self._make('{}/trends'.format(self.sink))

        # start plots
        self._stamp('plotting...', initial=True)

        # set default rows
        rows = rows or self.rows

        # collect all paths
        paths = self._see('{}/degradations'.format(self.sink))

        # begin averages reservoir
        averages = {}

        # go through each path
        for path in paths:

            # determine band and allocate for averages
            band = re.search('BAND[1-3]', path).group()

            # allocate for averages
            averages[band] = averages.setdefault(band, {})

            # open the hdf5 file
            five = self._fetch(path)

            # for each diffuser
            for code, description in list(self.diffusers.items()) + [(0, 'non_diffuser_components')]:

                # add averages entry
                averages[band][description] = averages[band].setdefault(description, {})

                # get the grid and drifts
                grid = five['Data']['wavelength_grid'][:]
                drifts = five['Data']['slopes'][:].transpose(1, 0)
                polynomials = five['Data']['drift_polynomials'][:].transpose(1, 0)

                # only for nonzero code
                if code > 0 and description in path:

                    # plot each row
                    lines = []
                    colors = ['r', 'g', 'b', 'm'] * len(rows)
                    for row, color in zip(rows, colors):

                        # if band 1
                        if 'BAND1' in band:

                            # divide by two for comparable location
                            row = int(row / 2)

                        # make a line
                        abscissa = [float(entry) for entry in grid]
                        ordinate = [float(wave[row]) for wave in drifts]
                        style = color + '-'
                        label = 'row_{}'.format(row)

                        # add line
                        line = (abscissa, ordinate, style, label)
                        lines.append(line)

                        # grab polynomial
                        polynomial = [float(wave[row]) for wave in polynomials]

                        # plot polynomial
                        style = color + '--'
                        line = (abscissa, polynomial, style, None)
                        lines.append(line)

                    # set texts
                    title = '{} {} drift'.format(band, description)
                    independent = 'wavelength (nm)'
                    dependent = '% drift / year'
                    text = [title, independent, dependent]

                    # create plot destination
                    formats = (self.sink, description, band, tuple(rows))
                    destination = '{}/trends/trends_{}_{}_rows{}.png'.format(*formats)

                    # plot
                    self._draw(lines, text, destination)

                # add to spatial and spectral average graphs
                averages[band][description]['grid'] = grid
                averages[band][description]['rows'] = five['Data']['{}_row_avg'.format(description)][:]
                averages[band][description]['spectrum'] = five['Data']['{}_spectral_avg'.format(description)][:]

                # get degradations
                degradations = five['Data'][description][:].transpose(1, 0)

                # plot each row
                lines = []
                colors = ['r', 'g', 'b', 'm'] * len(rows)
                for row, color in zip(rows, colors):

                    # if bnad 1
                    if 'BAND1' in band:

                        # divide by two for comparable location
                        row = int(row / 2)

                    # make a line
                    abscissa = [float(entry) for entry in grid]
                    ordinate = [float(wave[row]) for wave in degradations]
                    style = color + '-'
                    label = 'row_{}'.format(row)

                    # add line
                    line = (abscissa, ordinate, style, label)
                    lines.append(line)

                # set texts
                title = '{} {} degradation'.format(band, description)
                independent = 'wavelength (nm)'
                dependent = '% drift / year'
                text = [title, independent, dependent]

                # create plot destination
                formats = (self.sink, description, band, tuple(rows))
                destination = '{}/trends/calculated_degradation_{}_{}_rows{}.png'.format(*formats)

                # plot
                self._draw(lines, text, destination)

            # close hdf5 file
            five.close()

        # plot aggregate average plots
        self._consolidate(averages)

        # timestamp
        self._stamp('plotted.')

        return None

    def verify(self, band, diffuser, row, orbit):
        """Verify the solar irradiance interpolations by plotting together.

        Arguments:
            band: int, the band
            diffuser: int, the diffuser code
            row: int, the particular row
            orbit: int, the index of the orbit

        Returns:
            None
        """

        # get the particular fusion file and degradation file
        degradation = [path for path in self._see('{}/degradations'.format(self.sink)) if 'BAND' + str(band) in path][0]

        # get the diffuser
        description = self.diffusers[diffuser]

        # open the fusion file
        five = self._fetch(degradation)

        # get abscissas
        wavelengths = five[description]['calculated_wavelengths'][:][orbit][row]
        grid = five[description]['wavelength_grid'][:]

        # get ordinates
        irradiances = five[description]['irradiance_avg'][:][orbit][row]
        interpolations = five[description]['interpolated_irradiances'][:][orbit][row]

        # close file
        five.close()

        # begin lines
        lines = []

        # make line for measured irradiances
        line = [wavelengths, irradiances, 'k2-', 'irradiances']
        lines.append(line)

        # make line for interpolated irradiances
        line = [grid, interpolations, 'm1', 'interpolations']
        lines.append(line)

        # annotate graph
        formats = (band, diffuser, row, orbit)
        title = 'irradiances vs interpolations for band {}, diffuser {}, row {}, orbit {}'.format(*formats)
        independent = 'wavelength (nm)'
        dependent = 'irradiance'
        texts = [title, independent, dependent]

        # make destination
        destination = '{}/plots/verification_band{}_diffuser{}_row{}_orbit{}.png'.format(self.sink, *formats)

        # plot
        self._draw(lines, texts, destination)

        return None


# set up main function to run from command line
if __name__ == '__main__':

    # split inputs into options and arguments, padding out arguments with blanks
    options = [option for option in sys.argv[1:] if '-' in option]
    arguments = [argument for argument in sys.argv[1:] if '-' not in argument] + [''] * 10

    # specify the sink directory for dumping results
    sink = arguments[0]

    # specify the source file or directory for retrieving OMI BCAL files
    source = arguments[1]

    # optionally specify start and finish days of the month (eg., 01 31) for date based subdirectories
    start, finish = arguments[2:4]

    # setting postpone to true will postpone fusing individual rawgaze files into bigger fusion files
    postpone = any([option in options for option in ('-p', '--p', '-postpone', '--postpone')])

    # setting force to true will force a fusion
    force = any([option in options for option in ('-f', '--f', '-force', '--force')])

    # setting extend to true will extend the accumulation to all calibration files with any ic id
    extend = any([option in options for option in ('-e', '--e', '-extend', '--extend')])

    # setting cube to true will allow the entire arrays to get extracted instead of a single time slice
    cube = any([option in options for option in ('-c', '--c', '-cube', '--cube')])

    # setting degrade to true will calculate the degradation rates
    analyze = any([option in options for option in ('-a', '--a', '-analyze', '--analyze')])

    # create caladrius instance from arguments and options
    toggles = {'postpone': True, 'extend': extend, 'cube': cube, 'analyze': analyze, 'force': force}
    caladrius = Caladrius(sink, source, start=start, finish=finish, **toggles)

    # create single orbit, single band rawgaze files for all orbits in the source directories
    caladrius.gaze()

    # if there is no option to postpone
    if not caladrius.postpone or caladrius.force:

        # fuse all rawgaze files into fusion files
        caladrius.fuse()

    # if the degrade option is set
    if caladrius.analyze:

        # fuse all rawgaze files
        caladrius.fuse()

        # perform degradation calculations
        caladrius.calibrate()
        caladrius.diffuse()
        caladrius.degrade()



