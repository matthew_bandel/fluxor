# Fluxor

Fluxor is a Python program meant to extract a few fields from OMI Calibration files for the purposes of studying irradiance measurements and diffuser degradation.

#### Contents

There are six files:

- fluxors.py: main fluxors script
- fluxors.cpython-36.pyc: main fluxors script compiled as bytecode
- configuration.yaml: configuration file
- requirements.txt: list of required modules and their versions
- README.md: this document
- README.pdf: this document as a Katex-rendered pdf via the Notable app

#### Single File Use

Fluxor may be invoked from the commandline as follows:

```buildoutcfg
$ python fluxors.py sink/directory source/file.nc
```

The first argument is a directory path for dumping the extractions.  Two subdirectories will be created, "fluxes" for storing the single-orbit rawflux extractions, and "fusions" for storing the multi-orbit fusions of single orbit rawflux files.

The second argument is the path to a source OMI L1-OML1BCAL .nc file.

#### Multiple File Use

Alternatively, Fluxor may be invoked for a directory of multiple files in the same way:

```buildoutcfg
$ python fluxors.py sink/directory source/directory
```

In this case, the source/directory is assumed to contain multiple OMI L1-OML1BCAL files.  They will be fed one at a time through the fluxor.

#### Multiple Directory Use

Alternatively, Fluxor may be invoked for multiple directories, assuming the multiple directories are organized by date:

```buildoutcfg
$ python fluxors.py sink/directory source/directory/2011/03 05 15
```

In this case, all the relevant orbits in directories from Mar 5, 2011 through Mar 15, 2011 will be extracted.

#### Postpone option

There are two main parts to the extraction.  Firstly, single orbit rawflux extractions are made from each relevant orbit and stored in the "fluxes" directory.  Secondly, all rawflux files in this directory are fused together and placed in the "fusions" directory.  It may be desirable to postpone this second step until all rawfluxes have been gathered:

```buildoutcfg
$ python fluxors.py sink/directory source/directory --postpone
```

In this case, only the rawflux files will be generated, without fusing them further.

#### Extend option

As diffuser calibration orbits are rather sparse in the record (occuring only once per day for the most frequent, and once per month for the least frequent), the Fluxor checks for particular orbit instrument configuration ids before subsetting the data and storing it to disk.  However, it is possible to extend the extraction to all orbits as follows:

```buildoutcfg
$ python fluxors.py sink/directory source/directory --extend
```

In this case, all orbits will be extracted without regard to their particular instrument configuration ids.

#### Cube option

Fluxor was written to extract average values as in general the entire solar irradiance record along the track is not needed.  If a parameter has an entry for every point along the track, only particular images are taken instead of the full record.  If there is interest in gathering the entire trackwise data, the cube option may be used:

```buildoutcfg
$ python fluxors.py sink/directory source/directory --cube
```

In this case, all parameters will be extracted as full cubes instead of particular slices.

#### Configuration

The configuration details are found in the configuration.yaml file.  Its contents are as follows:

- collection: set to "Collection-4", only used for Rawflux file names
- product: set to "L1-OML1BCAL", only used for Rawflux file names
- mode: set to "SOLAR_IRRADIANCE", only data from some kind of SOLAR_IRRADIANCE mode will be extracted, including "SOLAR_IRRADIANCE_MODE_008", and any "SOLAR_IRRADIANCE_SPECIAL" modes
- diffusers: sets the particular ic ids of interest (in this case, the diffuser calibrations)
- angles: sets the particular solar elevation angles at which to subsample three dimensional trackwise data (where each given entry represents a +/- pair and 0.0 is sampled automatically).  
- rows: sets the particular detector rows for degradation plots (values get halved for Band1)
- parameters: list of irradiance and wavelength associated parameters, adding an 'X' after the parameter name will include it in the extractions

#### Program Overview

The main steps of the program are as follows:

#### Data Ingestion

All given directories are searched for .nc files.  One at a time, each file is ingested in that the contents are mapped without taking the time to get the numerical contents.  For further time savings, only data within a SOLAR IRRADIANCE group is scanned.

Next, an ic id is grabbed from the file and compared with the list of diffuser calibration ic ids.  If the ic id does not match, the file is aborted (unless the "extend" option is set).

#### Extraction into Rawflux

Based on the parameters selected in the configuration file, the numerical data for the parameters is extracted from the orbit file.  If the particular parameter is three dimensional, only a subset of the data is taken, in order to save file space.  Three dimensional data is assumed to have its first dimension along the orbital track.  The most representative image is assumed to be that closest to 0.0 degrees solar elevation angle. Additional angles may be specifed in the configuration file.  The "cube" option overrides this image slicing, and will store the full three dimensional cube.

Independent data is also extracted, i.e., the orbit number, the orbit start times, and sun-earth distance.

The data is further parsed by band, so one callibration orbit will generally produce three Rawflux files, one per band.

#### Fusion into Aggregates

Next, all individual rawflux files are fused into a single file (per band), that contains the extracted parameters for all calibration files analyzed.  

Sessions are cumulative.  Before fusing all files, the contents of the current aggregate files are explored.  New rawflux data for the same orbits will overwrite the prior data.  However, data with orbit numbers not present in the current session will be kept.  But if the set of parameters in the new data is found to be incompatible with the older data, the aggregate file will be created anew (and the incompatible data will be saved as a timestamped copy in an "archives" folder).  

The aggregate hdf5 files are structured in imitation of OMPS files.  This means there are three main groups, Categories, Data, and IndependentVariables.  The Categories contain the main parameters, structured in a hierarchy as they are in the original files.  The Data group contains redundant links to the same data, but without the hierarchical structure.  The IndependentVariables group contains the orbit number, time information, and sun earth distances.

Setting the "postpone" option will skip the fusion process.  

#### Space Estimates

The hdf5 "gzip" compression option (at a level of 4) is used for both the singlet files and aggregate files.  Under the current configuration, this results in files of approximately:

- 150 kB for BAND1
- 570 kB for BAND2
- 720 kB for BAND3

The three diffusers are calibrated on a daily, weekly, and monthly basis.  Assuming a mission length of 15 years, there are 6435 total files of interest (15 * 365 + 52 + 12).  The total estimated file sizes for the aggregates are:

- 0.9 GB for BAND1
- 3.5 GB for BAND2
- 4.4 GB for BAND3

#### Time Estimates

The time for extracting a single orbit for rawflux files is estimated at:

- 0.15 s to map the features
- 0.05 s for extracting BAND1
- 0.12 s for extracting BAND2
- 0.15 s for extracting BAND3

The time for fusing a single orbit into an aggregate is estimated at:

- 0.03 s for BAND1
- 0.06 s for BAND2
- 0.07 s for BAND3

The time for checking a file and aborting if it is not of interest:

- 0.05 s

For all 6435 orbits of interest, the total extraction time is estimated at:

- 55.8 minutes

The total fusion time is estimated at:

- 17.1 minutes

The extra time spend scanning and aborting for (14 * 6435) orbits not of interest:

- 75.1 minutes

Total estimated time for extracting entire mission:

- 148 minutes

#### Degradation Rates

Adding a "--degrade" flag will trigger a subsequent calculation of wavelength grids, interpolated solar irradiances, and estimation of diffuser degradation rates:  

```buildoutcfg
$ python fluxors.py sink/directory source/file.nc --degrade
```

Omitting the source directory will trigger the same process on the aggregate files already there:

```buildoutcfg
$ python fluxors.py sink/directory --degrade
```

#### Calculating Diffuser Degradation Rates

Diffuser degradation rates are calculated using the following steps:

#### Wavelength Calculation

The wavelength for each spatial and spectral pixel is calculated from the wavelength polynomial coefficients.  In this case, because coefficients are given for every image along the track, the coefficients are used from the image at a solar elevation angle closest to 0.0 degrees.  A short study suggested that differences in calculated wavelengths along the track vary on the order of 4e-4 nm for Band1, 5e-5 nm for Band2, and 2e-5 nm for Band3, and are therefore rather insensitive to the particular image used.  The wavelength for each pixel is calculated by:

$w_{ij} = \sum_{k}{c_{ik} * (j - r) ^ k}$

where,

$i$ is the spatial pixel row index
$j$ is the spectral pixel column index
$k$ is the index (and order) of the particular polynomial coefficient
$r$ is the reference column index
$w_{ij}$ is the wavelength at spatial row $i$ and spectral column $j$
$c_{ik}$ is the $k$th order polynomial coefficient for row $i$


References: 
- L1BCAL2RAWFLX.pro, Glen Jaross, lines 45-56

Deviations:
- In the reference, 4th order coefficients are skipped for Band1.
- In the reference, only one set of wavelength coefficients is extracted for each band, instead of one per orbit.


#### Wavelength Grid

In order to compare solar irradiances at the same wavelengths, measured solar irradiances are fit to a wavelength grid using linear interpolation.  A grid is constructed for every wavelength at 0.1 nm intervals.  Due to the spectral smile of the detectors, not every row of spatial pixels covers the same spectral range.  Only those wavelengths contained within all rows are used, with the spectral smiles effectively cropped out of the grid.

References:
- ReadNMRawFlux_NPP.py, Ramavarmaraja Mundakkara-Kovilakom, line 196

Deviations:
- In the reference, the wavelength grid is read in directly from the input Rawflux files, without details as to its preparation.


#### Solar Irradiance Interpolation

For each wavelength in the grid, the average solar irradiances from the pixels with the closest calculated wavelengths are used to estimate the irradiance at each grid point via linear interpolation:

$s_{il} = s_{ij} + (w_{l} - w_{ij}) * (s_{ij+1} - s_{ij}) / (w_{ij+1} - w_{ij})$

where,

$i$ is the spatial pixel row index
$j$, $j+1$ are the spectral pixel column indices that have wavelengths straddling the wavelength grid point
$l$ is the index of the wavelength grid point
$s_{il}$ is the interpolated solar irradiance at spatial row $i$ and wavelength grid point $l$
$w_{l}$ is the wavelength grid point at index $l$
$w_{ij}$ and $w_{ij+1}$ are the wavelengths at row $i$, columns $j$ and $j+1$, straddling the grid point
$s_{ij}$ and $s_{ij+1}$ are the average solar irradiances measured at these same pixels

References:
- The Ozone Monitoring Instrument, Levelt et. al. (2006), page 1099 

Deviations:
- The reference elucidates the need for interpolation to a wavelength grid, but warns even spline interpolation (let alone linear interpolation) is not accurate enough for some purposes. 


#### Linear Regression

For each diffuser, its subset of interpolated solar irradiances are normalized as a percent difference of the initial measurement:

$d_{iln} = 100 * (s_{iln} - s_{ilo}) / s_{ilo}$

where,

$d_{iln}$ is the difference between interpolated irradiances, at row $i$, wavelength $l$, and time $t_{n}$, scaled as a percent of the initial irradiance
$s_{iln}$ is the interpolated irradiance at row $i$, wavelength $l$, time $t_{n}$
$s_{ilo}$ is the initial interpolated solar irradiance at row $i$, wavelength $l$, initial time $t_{0}$

These percent differences are then fit to a weighted linear regression line. If the percent difference at a particular time is somewhat outlandish (in excess of +/- 10000 %) it is given a weight of 0.0.  Otherwise it is weighted at 1.0.  This effectively screens for the ~1e+36 fill values that signal faulty irradiance measurements (typical irradiance values are ~1e-6):

$y_{iln} = m_{il} (t_{n} - t_{0}) + b_{il}$

where,

$t_{0}$ is the initial time
$y_{iln}$ is the regression line's prediction for percent change at row $i$, wavelength $l$, time $t_{n}$
$m_{il}$ is the slope of this regression line with respect to $t$
$b_{il}$ is the intercept of this regression line at $t_{0}$

References:
- ReadNMRawFlux_NPP.py, Ramavarmaraja Mundakkara-Kovilakom, lines 265 - 302

Deviations:
- In the reference, saturated (fill value) irradiances are removed from the data prior 
to performing regression, to the same effect as using weights.


#### Spectral Polynomials

The slopes $m_{il}$ found for each wavelength can be fit to a third order polynomial via polynomial regression as a function of wavelength for each row:

$p_{il} = \sum_{k}{c_{ik} * m_{il} ^ k}$

where

$p_{il}$ is the polynomial best fit percent drift for row $i$ and wavelength $l$
$c_{ik}$ is the coefficient of $k$th order for row $i$
$m_{il}^k$ is the slope from the regression line for row $i$ and wavelength $l$ taken to $k$th power

References:
- ReadNMRawFlux_NPP.py, Ramavarmaraja Mundakkara-Kovilakom, lines 328 - 334


#### Solving Diffuser Degradation Rates

Because measurements are taken with three different diffusers, an estimation of overall instrument degradation may be calculated.  For instance, the following is a simplified model of measurements taken with the quartz volume diffuser, the diffuser responsible for the majority of solar irradiance measurements:

1] $p_{qil} = r_{qil} + r_{dil} + u_{il}$

where,

$p_{qil}$ is the polynomial best fit percent drift for measurements with the quartz volume diffuser, at row $i$ and wavelength $l$
$r_{qil}$ is the degradation rate of the quartz volumne diffuser itself, at row $i$ and wavelength $l$
$r_{dil}$ is the degradation rate of everything else in the detector, at row $i$ and wavelength $l$
$u_{il}$ is the unknown average trend in solar irradiance over the study period, for row $i$ and wavelength $l$

Two more equations follow analogously for measurements taken weekly with the aluminum diffuser:

2] $p_{ail} = r_{ail} + r_{dil} + u_{il}$

and monthly with the backup aluminum diffuser:

3] $p_{bil} = r_{bil} + r_{dil} + u_{il}$

Now there are three equations and five unknown variables.  A fourth equation makes use of the fact that both aluminum diffusers, being of the same material, can be expected to degrade at the same rate when normalized for total exposure time:

$x = \sum_{nb}{x_{b}} / \sum_{na}{x_{a}}$

where,

$x$ is the ratio of total exposure times for the aluminum diffusers
$x_{a}$ is the exposure time for each irradiance measurement with the aluminum diffuser over $n_{a}$ measurements
$x_{b}$ is the exposure time for each irradiance measurement with the backup diffuser over $n_{b}$ measurements

The degradation rates of the two aluminum diffusers are related by this ratio:

4] $r_{bil} = x * r_{ail}$

Finally, by assuming the unknown change in actual solar irradiance is negligle by comparison, the system is solvable:

5] $u_{il} \approx 0$

The solution for the degradation rates of each diffuser, as well as the remaining detector components, are given here:

$r_{ail} = (p_{ail} - p_{bil}) / (1 - x)$

$r_{bil} = x * r_{ail} = x * (p_{ail} - p_{bil}) / (1 - x)$

$r_{dil} = p_{bil} - r_{bil} = (p_{bil} - x * p_{ail}) / (1 - x)$

$r_{qil} = p_{qil} - r_{dil} = p_{qil} - (p_{bil} - x * p_{ail}) / (1 - x)$

References:
- ReadNMRawFlux_NPP.py, Ramavarmaraja Mundakkara-Kovilakom, lines 355 - 364
- In-flight Performance of the Ozone Monitoring Instrument, Schenkeveld et. al. (2017), page 1976
- https://www.nasa.gov/mission_pages/sunearth/solar-events-news/Does-the-Solar-Cycle-Affect-Earths-Climate.html


Deviations:
- In the python file reference, the ratio of datapoints for the reference diffuser versus the working diffuser is used as a proxy for the ratio of exposures.
- In the Schenkeveld reference, a ratio fo 32/7 is used for the ratio of exposure times.
- Also in Schenkeveld, the degradation rate of the backup diffuser is regarded as 0 due to limited use, though this is later corrected by some means.
- NASA's FAQ page on the solar cycle suggests the solar irradiance varies by at most 0.15 % over the course of its cycle.

#### Thank you!

Please feel free to direct any comments, questions, or other feedback to Matthew Bandel, at matthew.bandel@ssaihq.com
