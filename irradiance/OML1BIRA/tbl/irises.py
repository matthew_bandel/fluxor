# irises.py for the Hydra class to parse hdf files

# import local classes
from hydras import Hydra
from formulas import Formula
from features import Feature

# import general tools
import os
import sys

# import regex
import re

# import subprocess
import subprocess

# import time and datetime
import datetime

# import numpy
import numpy


# class Hydra to parse hdf files
class Iris(Hydra):
    """Iris class to run irradiance averager.

    Inherits from:
        cores.Core
    """

    def __init__(self, sink='', start='', days=10, bands=(1, 2, 3), azimuths=(18, 32), archive=10004, job=''):
        """Initialize an Iris instance.

        Arguments:
            sink: str, folder for output data
            start: str, start date in '%Ym%m%d' format
            days: int, number of days over which to average
            bands: list of ints (1, 2, 3), the particular bands
            azimuths: list of ints, the azimuth angle bounds
            job: str, name of job yaml

        Returns:
            None
        """

        # initialize the base Hydra instance
        Hydra.__init__(self)

        # set default attributes
        self.sink = sink or '../tmp'
        self.start = start or '2005m0101'
        self.days = days
        self.bands = bands
        self.azimuths = azimuths
        self.archive = archive
        self.job = job or '../tmp/job.yaml'

        # allocate for paths
        self.paths = []

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # display paths
        self._tell(self.paths)

        # create representation
        representation = ' <  Iris instance at: {} >'.format(self.sink)

        return representation

    def _adjust(self, job):
        """Adjust the formatting of the job yaml file.

        Arguments:
            job: str, yaml file name

        Returns:
            None
        """

        # open as text
        knowledge = self._know(job)
        for index, line in enumerate(knowledge):

            # check for brackets
            if '.nc' in line:

                # reconstruct
                knowledge[index] = '  ' + line.replace('[', '').replace(']', '')

        # reconstruct
        knowledge = ['---'] + knowledge
        self._jot(knowledge, job)

        return None

    def _generate(self):
        """Generate the job control file.

        Arguments:
            None

        Returns:
            None
        """

        # print status
        self._print('generating control yaml: {}...'.format(self.job))

        # begin yaml file with title
        yam = {}
        yam['title'] = 'OML1BIRA job control file'

        # construct inputs, for each path
        inputs = {}
        paths = self.paths
        digits = len(str(len(paths)))
        for index, path in enumerate(paths):

            # add entry
            name = 'file{}'.format(str(index).zfill(digits))
            inputs[name] = [['{}'.format(path)]]

        # add to yam
        yam['Input Files'] = inputs

        # construct output destination
        formats = (self.sink, self.archive, self.start, self.days)
        formats += (''.join([str(band) for band in self.bands]),)
        formats += (''.join([str(angle) for angle in self.azimuths]),)
        formats += (self._note(),)
        destination = [['{}/OMI-Aura_L1-OML1BIRA_AS{}_{}_{}days_bands{}_angles{}_{}.nc'.format(*formats)]]

        # add output entry
        outputs = {'OML1BIRA': destination}
        yam['Output Files'] = outputs

        # assemble runtime parameters
        parameters = {}
        parameters.update({'Bands': list(self.bands)})
        parameters.update({'MaxAverageCount': 0})
        parameters.update({'MaxAzimuthAngle': self.azimuths[1]})
        parameters.update({'MinAzimuthAngle': self.azimuths[0]})
        parameters.update({'ECSCollection': '04'})
        parameters.update({'ECSCollectionMinorVersion': '00'})
        parameters.update({'TargetOrbitNumber': 0})
        parameters.update({'MaxOrbitOffset': 1})
        parameters.update({'PGEVersion': '4.0.0'})
        parameters.update({'PGEName': 'OML1BIRA'})
        parameters.update({'ShortName': 'OML1BIRA'})

        # add to yam
        yam['Runtime Parameters'] = parameters

        # dispense to file
        self._dispense(yam, self.job)

        # adjust yaml formatting
        self._adjust(self.job)

        return None

    def _obtain(self):
        """Construct all file paths.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.paths
        """

        # construct sink directory
        self._make(self.sink)

        # load up file of quartz volume diffuser orbits
        quartz = self._load('quartz.json')

        # get all file paths
        paths = []
        self._print('collecting paths...')

        # convert to datetime
        start = datetime.datetime.strptime(self.start, '%Ym%m%d')

        # for each day
        for days in range(self.days):

            # find the datetime
            finish = start + datetime.timedelta(days=days)

            # create folder reference
            formats = (self.archive, str(finish.year), str(finish.month).zfill(2), str(finish.day).zfill(2))
            folder = '/tis/OMI/{}/OML1BCAL/{}/{}/{}'.format(*formats)

            # get all qvd paths and accumulate
            contents = self._see(folder)
            contents = [path for path in contents if '.nc' in path]

            # check for date in quarzes record
            date = '{}m{}{}'.format(str(finish.year), str(finish.month).zfill(2), str(finish.day).zfill(2))
            if date in quartz.keys():

                # pick only file with that orbit
                orbit = quartz[date]
                contents = [path for path in contents if orbit in path]
                contents.sort()
                paths += contents

            # otherwise
            else:

                # only keep those with mode 8
                contents = [path for path in contents if self._recognize(path) == 8]
                contents.sort()
                paths += contents

        # print status
        self._print('{} paths collected.\n'.format(len(paths)))

        # set attribute
        self._tell(paths)
        self.paths = paths

        return None

    def _recognize(self, path):
        """Determine if the ic id in the orbit is recognized as a diffuser calibration.

        Arguments:
            path: str, file path

        Returns:
            boolean, orbit recognized?
        """

        # print status
        print('inspecting {} for ic id...'.format(path))

        # assume the orbit is not recognized
        code = 0

        # open the hdf5 file
        five = self._fetch(path)

        # try to
        try:

            # construct route, assuming a certain structure
            band = 'BAND1_IRRADIANCE'
            mode = list(five[band].keys())[0]
            instrument = 'INSTRUMENT'
            configuration = 'instrument_configuration'

            # get the ic id code
            code = five[band][mode][instrument][configuration][:][0][0][0]

        # unless there is a missing key
        except KeyError:

            # in which case, it is not recoginized
            print('file structure not recognized')

        # regardless
        finally:

            # close file
            five.close()

        return code

    def _submit(self):
        """Submit the job.

        Arguments:
            None

        Returns:
            None
        """

        # print status
        formats = (self.days, self.start, self.job, self.sink)
        self._print('running averager for {} days, starting {}, job {} into {}...'.format(*formats))

        # add library path linl
        os.environ['LD_LIBRARY_PATH'] = '../lib'

        # construct call and run executable
        call = ['../bin/oml1_irradiance_averager.exe', self.job]
        self._print(str(call))
        subprocess.call(call)

        return None

    def breeze(self):
        """Create the control file and run the irradiance averager.

        Arguments:
            None

        Returns:
            None
        """

        # get all appropriate paths
        self._obtain()

        # generate the control file
        self._generate()

        # submit job
        self._submit()

        return None

    def gust(self, folder, old, middle, new):
        """Assemble stack of measurements into a noise asymptote study.

        Arguments:
            folder: str, file path to folder of aveerages
            old: str, last folder to change
            intermediate, str, folder for intermediate
            final: str, folder for final


        Returns:
            None
        """

        # create hydra for folder
        hydra = Hydra('{}/{}'.format(folder, old))

        # arrange files by their day lengths
        paths = hydra.paths
        durations = [int(re.search('[0-9]{1,4}days', path).group().strip('days')) for path in paths]

        # zip and arrange based on durations
        zipper = list(zip(paths, durations))
        zipper.sort(key=lambda pair: pair[1])

        # retrieve ordered paths
        paths = [pair[0] for pair in zipper]
        self._tell(paths)

        # merge all paths
        destination = '{}/{}/OML1BIRA_merge_{}.h5'.format(folder, middle, self._note())
        hydra.merge(paths, destination)

        # ingest the merge
        hydra.ingest(destination)

        # create bands and rows
        bands = {'band1': 30, 'band2': 60, 'band3': 60}

        # prepare for cascade, for each band
        categories = []
        independents = []
        for band in bands.keys():

            # get subset of features with the band
            features = [feature for feature in hydra if band in feature.slash.lower()]

            # begin formula
            formula = Formula()

            # transfer several features, removing trivial dimensions
            def squeezing(tensor): return tensor.squeeze()
            parameters = ['irradiance', 'irradiance_error', 'irradiance_noise', 'irradiance_average_count']
            for parameter in parameters:

                # add to formulation
                formula.formulate(parameter, squeezing, '{}_{}'.format(band, parameter), 'Categories')

            # create cascade
            cascade = hydra.cascade(formula, features)
            categories += cascade

            # begin new formula
            formula = Formula()

            # calculate the wavelength polynomials
            rows = bands[band]
            def splintering(tensor): return [numpy.round(tensor.mean(axis=0), 1)[row] for row in range(rows)]
            parameter = 'wavelength'
            names = ['{}_pixel_grid_{}'.format(band, str(row + 1).zfill(2)) for row in range(rows)]
            addresses = ['IndependentVariables'] * rows
            formula.formulate(parameter, splintering, names, addresses)

            # add cascade to independents
            cascade = hydra.cascade(formula, features)
            independents += cascade

        # create feature for averaging spans
        durations.sort()
        spans = Feature(['IndependentVariables', 'span'], numpy.array(durations))
        independents.append(spans)

        # stash
        analysis = '{}/{}/OML1BIRA_timespan_analysis_{}.h5'.format(folder, new, self._note())
        hydra._stash(categories + independents, analysis, 'Data')

        return None


# if commandline system arguments are found
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]

    # pad arguments with blanks as defaults
    arguments += [''] * 3
    arguments = arguments[:3]

    # unpack control file name, output directory, input directory, and subdirectory range
    sink, start, days = arguments

    # create iris instance
    iris = Iris(sink, start, int(days))

    # run the averager
    iris.breeze()
