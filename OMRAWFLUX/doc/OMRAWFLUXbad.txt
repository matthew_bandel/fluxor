
ESDT Name: OMRAWFLUXbad

Processing Level: L1B

Long Name: OMI/Aura Irradiance for Backup Aluminum Diffuser from OML1BCAL files, 1-Orbit

Description: Single orbit extractions of averaged irradiance data, both corrected
      and uncorrected, from OML1BCAL files, for purposes of tracking diffuser degradation
      over time.

Science Team Contacts:

    - Matthew Bandel (matthew.bandel@ssaihq.com)

    - David Haffner (david.haffner@ssaihq.com)

Support Contact: Phillip Durbin (pdurbin@sesda.com)

Minimum Size: 50

Maximum Size: 1 MB

Filename Pattern: <Instrument>-<Platform>_<Processing Level>-<ESDT Name>_<DataDate>-o<OrbitNumber>_v<Collection>-<ProductionTime>.<File
      Format>

Platform: Aura

Instrument: OMI

File Format: h5

Period: Orbits=1

Archive Method: Compress

File SubTable Type: L1L2

Extractor Type: Filename_L1L2

Metadata Type: orbital

Parser: filename

keypattern: <OrbitNumber>
